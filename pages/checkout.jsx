import { useContext, useState, useEffect, useRef } from "react";
import { useRouter } from "next/router";
import Swal from "sweetalert2";
import axios from "axios";
import { Layout } from "../src/components/layout/Layout";
// import { TestElementsForm } from '../src/components/testElementsForm/TestElementsForm';
import { MainContainer } from "../src/components/assistent/assistentStyles";
import { CartItemList } from "../src/components/cartItemList/CartItemList";
import CartContext from "../src/context/cart/CartContext";
import { PaypalButton } from "../src/components/paypalButton/PaypalButton";
import {
	CartContainer,
	CartProductsList,
	CartStripeElements,
} from "../src/components/cartItemList/cartItemListStyles";
import { useScrollPosition } from "../src/hooks/useScrollPosition";
import { ButtonToTop } from "../src/components/buttonToTop/ButtonToTop";
import AppContext from "../src/context/AppContext";
import { Spinner } from "../src/components/spinner/Spinner";
import { EmptyCart } from "../src/components/emptyCart/EmptyCart";
import { MainSpinnerContainer } from "../src/components/globalStyles";
import { options } from "../utils/constants";
import * as gtag from '../utils/gtag'

const Checkout = () => {
	const {
		cartItems,
		totalToPay,
		clearCart,
		setInvoiceData,
		setTotalForPaypal,
	} = useContext(CartContext);
	const { user, isChecking } = useContext(AppContext);
	const router = useRouter();
	const [amountForPaypal, setAmountForPaypal] = useState(1);
	const [showPaypalButton, setShowPaypalButton] = useState(false);
	const [currentUsdValue, setCurrentUsdValue] = useState(0);
	const [isCreatingInvoice, setIsCreatingInvoice] = useState(false);
	const isMounted = useRef(true)

	const scrollPosition = useScrollPosition();

	useEffect(() => {
		return () => {
			isMounted.current = false
		}
	}, [])

	useEffect(() => {
		const convertToDollar = async () => {
			setShowPaypalButton(false);
			const totalToConvert = Math.round(totalToPay);
			console.log(totalToConvert, totalToPay);
			try {
				let usdValue = currentUsdValue;
				if (currentUsdValue === 0) {
					// const response = await axios.get(
					// 	`https://api.allorigins.win/raw?url=${encodeURIComponent(
					// 		`https://api.cambio.today/v1/quotes/GTQ/USD/json?quantity=${totalToConvert}&key=9781|7z44wBMf0VH0p5P*Z3WPyCf01Ggocyyw`
					// 	)}`
					// );
					const response = await axios.get(`https://api.allorigins.win/raw?url=${encodeURIComponent(`https://api.cambio.today/v1/quotes/GTQ/USD/json?quantity=${totalToConvert}&key=10804|gCTNOqX0wHqLNQsUt6pbmgDOJW2byvfX`)}`)
					console.log("Estoy consultando a la api");
					usdValue = response.data.result.value.toFixed(2) ?? 0.13;
				}

				const newAmount = Number(totalToPay) * Number(usdValue ?? 0.13);
				const roundedAmount = Number(newAmount.toFixed(2));

				console.log(roundedAmount, usdValue);
				if(isMounted.current){
					setAmountForPaypal(roundedAmount);
					setTotalForPaypal(roundedAmount);
					setCurrentUsdValue(usdValue);
					setTimeout(() => {
						setShowPaypalButton(true);
					}, 300);
				}
			} catch (error) {
				console.log(error);
			}
		};
		if (!isChecking) {
			if (!user) {
				router.replace("/login");
				return;
			} else {
				if (totalToPay > 0) {
					convertToDollar();
				}
			}
		}
	}, [totalToPay, isChecking, user]);

	const createInvoice = async (newCartItems, amount) => {
		// console.log("Diferencias en cart", newCartItems, cartItems);
		Swal.fire(
			"Pago éxitoso",
			"El asesor se comunicará contigo cuando tu producto esté listo.",
			"success"
		);
		setIsCreatingInvoice(true);
		const details = newCartItems.map((item) => ({
			id_producto: item.id_producto,
			cantidad: item.quantity,
		}));
		const detailsForPixel = newCartItems.map((item) => ({
			id: item.id_producto,
			quantity: item.quantity,
		}));
		const dataToSend = {
			id_cliente: user.userId,
			detalle: details,
		};
		try {
			const response = await axios.post(
				"https://gamersdungeon-api.herokuapp.com/products-api/v1/invoices",
				dataToSend
			);
			await axios.post('https://gamersdungeon-api.herokuapp.com/products-api/v1/mail/after-checkout', {
				"email_to": user.email
			})
			const data = response.data;
			const getInvoiceInfo = await axios.get(
				`https://gamersdungeon-api.herokuapp.com/products-api/v1/invoices/${data.id_factura}`
			);
			const invoice = getInvoiceInfo.data;
			import('react-facebook-pixel')
      .then(module => module.default)
      .then(ReactPixel => {
        ReactPixel.init('531836734572627', null, options)
        ReactPixel.pageView()
        ReactPixel.track('Purchase', {currency: 'USD', value: amount})
        console.log('Purchase');
      })

			gtag.event({
				action: 'purchase',
				category: 'ecommerce',
				label: 'Purchase',
				value: amount
			})

			// console.log(getInvoiceInfo.data, data);
			setInvoiceData(invoice);
			clearCart();
			router.replace("/pedidos");
			window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
		} catch (error) {
			console.log(error);
		}
	};

	return (
		<Layout>
			{!isCreatingInvoice ? (
				<MainContainer>
					{totalToPay !== 0 ? (
						<CartContainer>
							<CartProductsList>
								<CartItemList />
							</CartProductsList>
							<CartStripeElements>
								<h2>Pagar</h2>
								{showPaypalButton ? (
									<PaypalButton
										amountForPaypal={amountForPaypal}
										createInvoice={createInvoice}
										totalToPay={totalToPay}
										usdValue={currentUsdValue}
									/>
								) : (
									<Spinner size="20" color="#303030" />
								)}
							</CartStripeElements>
						</CartContainer>
					) : (
						<EmptyCart />
					)}
				</MainContainer>
			) : (
				<MainSpinnerContainer>
					<Spinner size="75" color="#303030" />
				</MainSpinnerContainer>
			)}
			{scrollPosition > 500 && <ButtonToTop />}
		</Layout>
	);
};

export default Checkout;
