import { useContext, useEffect, useState } from "react"
import { useRouter } from "next/router"
import axios from "axios"
import moment from 'moment'
import { MainSpinnerContainer } from "../src/components/globalStyles"
import { Layout } from "../src/components/layout/Layout"
import { OrdersList } from "../src/components/orders/OrdersList"
import { Spinner } from "../src/components/spinner/Spinner"
import AppContext from "../src/context/AppContext"
import { NoOrders } from "../src/components/orders/NoOrders"
import { getOrderStatus } from "../utils/getOrderStatus"

const Pedidos = () => {
  const { user, isChecking } = useContext(AppContext)

  const [ordersList, setOrdersList] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const router = useRouter()

  useEffect(() => {

    const getInvoices = async () => {
      const invoicesResponse = await axios.get(`https://gamersdungeon-api.herokuapp.com/products-api/v1/invoices/user/${user.userId}/history`)

      const invoicesData = invoicesResponse.data
      let fullInvoicesData = []

      for (let index = 0; index < invoicesData.length; index++) {
        const response = await axios.get(`https://gamersdungeon-api.herokuapp.com/products-api/v1/invoices/${invoicesData[index].id_factura}`)
  
        const details = response.data.detalle
        const create_date = response.data.fecha_factura
        const past_time = moment().diff(moment(create_date), 'days')
        // console.log(past_time);

        // console.log('Datos de la factura', invoicesData[index], details);

        const { status, orderType } = getOrderStatus(details.length, invoicesData[index].total_venta, past_time)

        invoicesData[index].details = details
        invoicesData[index].past_time = past_time
        invoicesData[index].status = status
        invoicesData[index].orderType = orderType
        
        fullInvoicesData = [...fullInvoicesData, { ...invoicesData[index]}]
      }

      const ordersSortedByDate = fullInvoicesData.sort((left, right) => (
        moment(right.fecha_factura).diff(moment(left.fecha_factura))
      ))

      // console.log('Final Result', invoicesData, fullInvoicesData);
      setOrdersList(ordersSortedByDate)
      setIsLoading(false)
    }
    if(!isChecking){
      if(user){
        getInvoices()
      }else {
        router.replace('/login')
      }
    }
  }, [isChecking, user])

  return (
    <Layout>
      {isLoading ? (
        <MainSpinnerContainer>
          <Spinner size='75' color='#303030'/>
        </MainSpinnerContainer>
      ): (
        <>
          {ordersList.length > 0 ? <OrdersList orders={ordersList}/> : <NoOrders/>}
        </>
      )}
    </Layout>
  )
}

export default Pedidos