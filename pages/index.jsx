import { Banner } from '../src/components/banner/Banner';
import { MainContainer } from '../src/components/globalStyles';
import { Layout } from '../src/components/layout/Layout';
import { MainTitleSection } from '../src/components/ProductsSectionsStyles';
import { ServicesHomeSection } from '../src/components/servicesHomeSection/ServicesHomeSection';
import HomeVideoSection from '../src/components/homeVideo/HomeVideoSection';

const Home = ()  => {

  return (
    <Layout>
      <Banner/>
      <HomeVideoSection/>
      <MainContainer>
      <MainTitleSection>¿Qué necesitas?</MainTitleSection>
        <ServicesHomeSection/>
      </MainContainer>
    </Layout>
  )
}

export default Home;
