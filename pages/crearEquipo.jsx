import { useRouter } from 'next/router'
import { useContext, useEffect, useState } from 'react'
import { AssistentComponent } from '../src/components/assistent/AssistentComponent'
import { MainSpinnerContainer } from '../src/components/globalStyles'
import { Layout } from '../src/components/layout/Layout'
import { Spinner } from '../src/components/spinner/Spinner'
import AppContext from '../src/context/AppContext'

const CrearEquipo = () => {
  const { user, isChecking } = useContext(AppContext)
  const router = useRouter()
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    if(!isChecking){
      if(!user){
        router.replace('/login');
      }
    }
  }, [])

  useEffect(() => {
    setIsLoading(isChecking)
  }, [isChecking])

  return (
    <Layout>
      {isLoading ? (
        <MainSpinnerContainer>
          <Spinner size='75' color='#303030'/>
        </MainSpinnerContainer>) 
        : (
        <AssistentComponent/>
      )}
    </Layout>
  )
}

export default CrearEquipo;