// import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react"
import { MainSpinnerContainer } from "../src/components/globalStyles"
import { InvoiceContainer } from "../src/components/invoice/InvoiceContainer"
import { Layout } from "../src/components/layout/Layout"
import { Spinner } from "../src/components/spinner/Spinner"
import AppContext from "../src/context/AppContext"

const Factura = () => {
  const { isChecking } = useContext(AppContext)
  // const router = useRouter()
  const [isLoading, setIsLoading] = useState(true);

  // useEffect(() => {
  //   if(!isChecking){
  //     if(!user){
  //       router.replace('/login')
  //     }
  //   }
  // }, [])

  useEffect(() => {
    setIsLoading(isChecking)
  }, [isChecking])

  return (
    <Layout>
      {isLoading ? (
        <MainSpinnerContainer>
          <Spinner size='75' color='#303030'/>
        </MainSpinnerContainer>) 
        : (
        <InvoiceContainer/>
      )}
    </Layout>
  )
}

export default Factura