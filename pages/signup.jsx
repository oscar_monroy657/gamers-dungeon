import { useRouter } from 'next/router'
import { useContext, useEffect, useState } from 'react'
import { SignupForm } from "../src/components/forms/signupForm/SignupForm"
import { LoginCover, LoginPageContainer, MainSpinnerContainer } from "../src/components/globalStyles"
import { Layout } from "../src/components/layout/Layout"
import { Spinner } from '../src/components/spinner/Spinner'
import AppContext from '../src/context/AppContext'

const Signup = () => {
  const { user, isChecking } = useContext(AppContext)
  const [isLoading, setIsLoading] = useState(true)
  const router = useRouter()

  useEffect(() => {
    if(!isChecking){
      if(user){
        router.replace('/crearEquipo');
      }
    }
  }, [isChecking])

  useEffect(() => {
    setIsLoading(isChecking)
  }, [isChecking])

  return (
    <Layout>
      {isLoading ? (
        <MainSpinnerContainer>
          <Spinner size='75' color='#303030'/>
        </MainSpinnerContainer>
      ) : (
      <LoginPageContainer>
        <SignupForm/>
        <LoginCover>
          <img src="/assets/account.svg" alt="cover"/>
        </LoginCover>
      </LoginPageContainer>)}
    </Layout>
  )
}

export default Signup
