import { ContactContent } from "../src/components/contact/ContactContent";
import { Layout } from "../src/components/layout/Layout";

const Contacto = () => {
  return (
    <Layout>
      <ContactContent/>
    </Layout>
  )
}

export default Contacto;
