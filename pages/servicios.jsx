import { Layout } from "../src/components/layout/Layout";
import { ServicesContent } from "../src/components/services/ServicesContent";

const Servicios = () => {
  return (
    <Layout>
      <ServicesContent/>
    </Layout>
  )
}

export default Servicios;
