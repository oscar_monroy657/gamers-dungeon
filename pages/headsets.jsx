import { useContext } from "react"
import { MainContainer } from "../src/components/assistent/assistentStyles"
import { ButtonToTop } from "../src/components/buttonToTop/ButtonToTop"
import { MainSpinnerContainer } from "../src/components/globalStyles"
import { Layout } from "../src/components/layout/Layout"
import { ProductsContainer } from "../src/components/listOfProducts/listProductsStyles"
import { ProductItem } from "../src/components/productItem/ProductItem"
import { TitleSection } from "../src/components/ProductsSectionsStyles"
import { Spinner } from "../src/components/spinner/Spinner"
import SomePiecesContext from "../src/context/somePieces/SomePiecesContext"
import { useScrollPosition } from "../src/hooks/useScrollPosition"

const Headsets = () => {
  const { isFetching, headsets,  } = useContext(SomePiecesContext)
	const scrollPosition = useScrollPosition()

  return (
    <Layout>
      <MainContainer>
        {isFetching ? (
          <MainSpinnerContainer height={50}>
          <Spinner size="45" color="#303030" />
        </MainSpinnerContainer>
        ): (
          <>
          <TitleSection>Headsets ({headsets.length})</TitleSection>
						<ProductsContainer>
							{headsets.map((motherBoard) => (
									<ProductItem
										key={motherBoard.id_producto}
										item={motherBoard}
									/>
								))}
						</ProductsContainer>
          </>
        )}
        {scrollPosition > 500 && <ButtonToTop/>}
      </MainContainer>
    </Layout>
  )
}

export default Headsets