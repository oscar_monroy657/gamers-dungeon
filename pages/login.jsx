import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react"
import { LoginForm } from "../src/components/forms/loginForm/LoginForm"
import { LoginCover, LoginPageContainer, MainSpinnerContainer } from "../src/components/globalStyles"
import { Layout } from "../src/components/layout/Layout"
import { Spinner } from "../src/components/spinner/Spinner"
import AppContext from "../src/context/AppContext"
import RobotContext from '../src/context/robot/RobotContext';
import { robotMessages } from '../src/utils/robotMessages/robotMessages';
import { RobotMessage } from '../src/components/robotMessage/RobotMessage';
import { useModal } from '../src/hooks/useModal';

const Login = () => {
  const { user, isChecking } = useContext(AppContext)
  const { showRobotLoginModal, setHideModal } = useContext(RobotContext)
  const [isLoading, setIsLoading] = useState(true)
  const [ isOpen, , closeModal ] = useModal(showRobotLoginModal)
	const messageForAssintant = robotMessages.login.message()
  const router = useRouter()

  useEffect(() => {
    if(!isChecking){
      if(user){
        router.replace('/crearEquipo');
      }
    }
  }, [isChecking])

  useEffect(() => {
    setIsLoading(isChecking)
  }, [isChecking])

  const handleClose = () => {
    setHideModal()
    closeModal()
  }

  return (
    <Layout>
      {isLoading ? (
        <MainSpinnerContainer>
          <Spinner size='75' color='#303030'/>
        </MainSpinnerContainer>
      ) :(
        <LoginPageContainer>
          <RobotMessage isOpen={isOpen} closeModal={handleClose} message={messageForAssintant} noShowLink={true}/>
          <LoginForm/>
          <LoginCover>
            <img src="/assets/account.svg" alt="cover"/>
          </LoginCover>
        </LoginPageContainer>
      )}
    </Layout>
  )
}

export default Login
