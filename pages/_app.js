import { useEffect } from "react";
import { Router, useRouter } from "next/router";
import Script from "next/script";
import { PayPalScriptProvider } from "@paypal/react-paypal-js";
import AppState from "../src/context/AppState";
import CartState from "../src/context/cart/CartState";
import RobotState from "../src/context/robot/RobotState";
import { SnackBarState } from "../src/context/snackbar/SnackBarState";
import { SomePiecesState } from "../src/context/somePieces/SomePiecesState";
import { PAYPAL_CLIENT_ID } from "../utils/constants";
import * as gtag from '../utils/gtag'

const FacebookPixel = () => {
	const options = {
		autoConfig: true, // set pixel's autoConfig. More info: https://developers.facebook.com/docs/facebook-pixel/advanced/
		debug: false, // enable logs
	};

	useEffect(() => {
		import("react-facebook-pixel")
			.then((x) => x.default)
			.then((ReactPixel) => {
				ReactPixel.init("531836734572627", null, options);
				ReactPixel.pageView();

				Router.events.on("routeChangeComplete", () => {
					console.log("React pixel ready");
					ReactPixel.pageView();
				});
			});
	});
	return null;
};

function MyApp({ Component, pageProps }) {
  const router = useRouter()
  useEffect(() => {
    const handleRouteChange = (url) => {
      gtag.pageview(url)
    }
    router.events.on('routeChangeComplete', handleRouteChange)
    return () => {
      router.events.off('routeChangeComplete', handleRouteChange)
    }
  }, [router.events])

	return (
		<AppState>
			<SomePiecesState>
				<CartState>
					<RobotState>
						<SnackBarState>
							<PayPalScriptProvider
								options={{ "client-id": PAYPAL_CLIENT_ID.clientId }}
							>
								<FacebookPixel />
								<Script
									strategy="afterInteractive"
									src={`https://www.googletagmanager.com/gtag/js?id=${gtag.GA_TRACKING_ID}`}
								/>
								<Script
									id="gtag-init"
									strategy="afterInteractive"
									dangerouslySetInnerHTML={{
										__html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${gtag.GA_TRACKING_ID}', {
              page_path: window.location.pathname,
            });
          `,
									}}
								/>
								<Component {...pageProps} />
							</PayPalScriptProvider>
						</SnackBarState>
					</RobotState>
				</CartState>
			</SomePiecesState>
		</AppState>
	);
}

export default MyApp;
