import { useContext } from "react"
import { MainContainer } from "../src/components/assistent/assistentStyles"
import { ButtonToTop } from "../src/components/buttonToTop/ButtonToTop"
import { MainSpinnerContainer } from "../src/components/globalStyles"
import { Layout } from "../src/components/layout/Layout"
import { ProductsContainer } from "../src/components/listOfProducts/listProductsStyles"
import { ProductItem } from "../src/components/productItem/ProductItem"
import { TitleSection } from "../src/components/ProductsSectionsStyles"
import { Spinner } from "../src/components/spinner/Spinner"
import SomePiecesContext from "../src/context/somePieces/SomePiecesContext"
import { useScrollPosition } from "../src/hooks/useScrollPosition"
import { useModal } from '../src/hooks/useModal';
import { robotMessages } from '../src/utils/robotMessages/robotMessages';
import { RobotMessage } from '../src/components/robotMessage/RobotMessage';
import {
    RobotContainer,
    RobotIconContainer
} from '../src/components/robotMessage/robotMessageStyles';

const Desks = () => {
  const { isFetching, desks,  } = useContext(SomePiecesContext)
  const [ isOpen, openModal, closeModal ] = useModal(false)
	const messageForAssintant = robotMessages.desks.messages()

	const scrollPosition = useScrollPosition()

  return (
    <Layout>
      <MainContainer>
        {isFetching ? (
          <MainSpinnerContainer height={50}>
          <Spinner size="45" color="#303030" />
        </MainSpinnerContainer>
        ): (
          <>
            <RobotMessage isOpen={isOpen} closeModal={closeModal} message={messageForAssintant} noShowLink={true}/>
            <RobotContainer>
              <TitleSection>Escritorios ({desks.length})</TitleSection>
              <RobotIconContainer onClick={openModal} ml>
                <img src="/assets/bot.svg" alt="asistente online" />
              </RobotIconContainer>
            </RobotContainer>
						<ProductsContainer>
							{desks.map((motherBoard) => (
									<ProductItem
										key={motherBoard.id_producto}
										item={motherBoard}
									/>
								))}
						</ProductsContainer>
          </>
        )}
        {scrollPosition > 500 && <ButtonToTop/>}
      </MainContainer>
    </Layout>
  )
}

export default Desks