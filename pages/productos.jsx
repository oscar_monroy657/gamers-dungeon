import {
	MainContainer,
	OptionIconContainer,
	OptionsContainer,
} from "../src/components/assistent/assistentStyles";
import { ButtonToTop } from "../src/components/buttonToTop/ButtonToTop";
import { Layout } from "../src/components/layout/Layout";
import { useScrollPosition } from "../src/hooks/useScrollPosition";
import { ServicesHomeItem } from "../src/components/servicesHomeSection/servicesHomeSectionStyles";
import { ItemTitle } from "../src/components/listOfProducts/productItemStyles";
import { useRouter } from "next/router";

const Productos = () => {
	const scrollPosition = useScrollPosition();
	const router = useRouter();

	return (
		<Layout>
			<MainContainer>
				<OptionsContainer>
					<ServicesHomeItem
						mr
						onClick={() => router.push("/motherboards")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/motherboard.svg" alt="Tarjetas madre" />
						</OptionIconContainer>
						<ItemTitle>Tarjetas madre</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/processors")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/cpu.svg" alt="Procesadores" />
						</OptionIconContainer>
						<ItemTitle>Procesadores</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/rams")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/ram-memory.svg" alt="Memorias ram" />
						</OptionIconContainer>
						<ItemTitle>Memorias RAM</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/graphicsCard")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/graphics-card.svg" alt="Tarjetas gráficas" />
						</OptionIconContainer>
						<ItemTitle>Tarjetas gráficas</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/liquidCoolers")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/cooler.svg" alt="Enfriamiento Líquido" />
						</OptionIconContainer>
						<ItemTitle>Enfriamiento líquido</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/powerSources")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/pc-tower.svg" alt="Fuentes de Poder" />
						</OptionIconContainer>
						<ItemTitle>Fuentes de poder</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/hdds")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/hard-disk.svg" alt="Discos duros" />
						</OptionIconContainer>
						<ItemTitle>Discos duros</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/ssds")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/ssd.svg" alt="Discos de estado sólido" />
						</OptionIconContainer>
						<ItemTitle>Discos de estado sólido</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/operatingSystems")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img
								src="/assets/operating-system.svg"
								alt="Sistemas operativos"
							/>
						</OptionIconContainer>
						<ItemTitle>Sistemas operativos</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/keyboards")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/keyboard.svg" alt="Teclados" />
						</OptionIconContainer>
						<ItemTitle>Teclados</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/mouses")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/mouse.svg" alt="Mouses" />
						</OptionIconContainer>
						<ItemTitle>Mouses</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/screens")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/screen.svg" alt="Monitores" />
						</OptionIconContainer>
						<ItemTitle>Monitores</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/headsets")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/headset.svg" alt="Headsets" />
						</OptionIconContainer>
						<ItemTitle>Headsets</ItemTitle>
					</ServicesHomeItem>

					<ServicesHomeItem
						mr
						onClick={() => router.push("/desks")}
						pointer
						width={20}
					>
						<OptionIconContainer>
							<img src="/assets/desk.svg" alt="Escritorios" />
						</OptionIconContainer>
						<ItemTitle>Escritorios</ItemTitle>
					</ServicesHomeItem>
				</OptionsContainer>
				{scrollPosition > 500 && <ButtonToTop />}
			</MainContainer>
		</Layout>
	);
};

export default Productos;
