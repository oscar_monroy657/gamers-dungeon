export const getOrderStatus = (detailArrayLength, total, past_time) => {
  let status = 'checking'
  let orderType = 'complete_building'

  if(detailArrayLength > 6){
    if(past_time > 5) status = 'building'
    if(past_time > 13) status = 'on route'
    if(past_time >= 15) status = 'delivered'
  }
  if(detailArrayLength < 6 && Number(total) >= 2499){
    if(past_time > 3) status = 'preparing'
    if(past_time > 5) status = 'on route'
    if(past_time >= 7) status = 'delivered'
    
    orderType = 'many_products'
  }
  if(detailArrayLength < 6 && Number(total) < 2499){
    if(past_time > 2) status = 'on route'
    if(past_time > 3) status = 'delivered'

    orderType = 'single_products'
  }

  return {status, orderType}
}