import { dataToSendTemplate } from "../dataToSendTemplate"

export const convertDataToSend = (state, accessoriesToSend) => {

  const challengesOptions = state.challenges.map(challenge => ({
    "respuesta": challenge,
    "nombre_visual_respuesta": challenge,
    "seleccionada": true
  }))
  const budgetMin = {
    "respuesta": "budgetMin",
    "nombre_visual_respuesta": state.budget.min,
    "seleccionada": true
  }
  const budgetMax = {
    "respuesta": "budgetMax",
    "nombre_visual_respuesta": state.budget.max,
    "seleccionada": true
  }
  const operatingSystem = {
    "respuesta": state.operatingSystem,
    "nombre_visual_respuesta": state.operatingSystem,
    "seleccionada": true
  }
  const manufacturer = {
    "respuesta": state.manufacturer,
    "nombre_visual_respuesta": state.manufacturer,
    "seleccionada": true
  }

  const accessories = accessoriesToSend.map(accessory => ({
    "respuesta": accessory,
    "nombre_visual_respuesta": accessory,
    "seleccionada": true
  }))

  const dataToSend = dataToSendTemplate
  if(accessories.length > 0){
    dataToSend.respuestas_paso = [
      ...challengesOptions, budgetMin, budgetMax, operatingSystem, manufacturer, ...accessories
    ]
  }else {
    dataToSend.respuestas_paso = [...challengesOptions, budgetMin, budgetMax, operatingSystem, manufacturer]
  }

  return dataToSend
}