import { RobotTypes } from "./robotTypes";

const initialState = {
  showRobotLoginModal: false,
  pageToShow: '/crearEquipo'
}

const robotReducer = (state = initialState, action) => {
  switch (action.type) {
    case RobotTypes.SET_SHOW_LOGIN_ROBOT_MODAL:
      return {
        ...state,
        showRobotLoginModal: true,
      }
    case RobotTypes.SET_HIDE_LOGIN_ROBOT_MODAL:
      return {
        ...state,
        showRobotLoginModal: false,
      }
    case RobotTypes.SET_PAGE_TO_SHOW:
      return {
        ...state,
        pageToShow: action.payload
      }
    default:
      return state
  }
}

export default robotReducer