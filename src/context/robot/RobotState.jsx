import { useReducer } from "react"
import RobotContext from "./RobotContext"
import robotReducer from "./robotReducer"
import { RobotTypes } from "./robotTypes"

const RobotState = ({ children }) => {
  const initialState = {
    showRobotLoginModal: false,
    pageToShow: '/crearEquipo'
  }

  const [state, dispatch] = useReducer(robotReducer, initialState)

  const setShowModal = () => {
    dispatch({
      type: RobotTypes.SET_SHOW_LOGIN_ROBOT_MODAL
    })
  }

  const setHideModal = () => {
    dispatch({
      type: RobotTypes.SET_HIDE_LOGIN_ROBOT_MODAL
    })
  }

  const setPageToShow = (pageUrl) => {
    dispatch({
      type: RobotTypes.SET_PAGE_TO_SHOW,
      payload: pageUrl
    })
  }

  return (
    <RobotContext.Provider value={{
      showRobotLoginModal: state.showRobotLoginModal,
      pageToShow: state.pageToShow,
      setShowModal,
      setHideModal,
      setPageToShow
    }}>
      {children}
    </RobotContext.Provider>
  )
}

export default RobotState