import { useEffect, useReducer } from 'react'
import axios from 'axios'
import SomePiecesContext from './SomePiecesContext'
import somePiecesReducer from './somePiecesReducer'
import { SomePiecesTypes } from './somePiecesTypes'
import { filterProducts } from './utils/filterProducts'
import { KeywordsToFilter } from './utils/keywordsToFilter'

export const SomePiecesState = ({children}) => {
  const initialState = {
    pieces: [],
    powerSources: [],
    motherBoards: [],
    processors: [],
    ssds: [],
    hdds: [],
    rams: [],
    keyboards: [],
    mouses: [],
    graphicsCards: [],
    liquidCoolers: [],
    screens: [],
    headsets: [],
    operatingSystems: [],
    desks: [],
    isFetching: false,
    isFirstTime: true,
    error: false,
    message: null,
  }

  const [state, dispatch] = useReducer(somePiecesReducer, initialState)

  const getPieces = async () => {
    dispatch({
      type: SomePiecesTypes.GET_PIECES_START
    })
    try {
      const pieces = await axios.get('https://gamersdungeon-api.herokuapp.com/products-api/v1/products?size=500')
      const allProducts = pieces.data
      const powerSources = filterProducts(KeywordsToFilter.powerSources, allProducts)
      const motherBoards = filterProducts(KeywordsToFilter.motherBoards, allProducts)
      const processors = filterProducts(KeywordsToFilter.processors, allProducts)
      const ssds = filterProducts(KeywordsToFilter.ssds, allProducts)
      const hdds = filterProducts(KeywordsToFilter.hdds, allProducts)
      const rams = filterProducts(KeywordsToFilter.rams, allProducts)
      const keyboards = filterProducts(KeywordsToFilter.keyboards, allProducts)
      const mouses = filterProducts(KeywordsToFilter.mouses, allProducts)
      const graphicsCards = filterProducts(KeywordsToFilter.graphicsCards, allProducts)
      const liquidCoolers = filterProducts(KeywordsToFilter.liquidCoolers, allProducts)
      const screens = filterProducts(KeywordsToFilter.screens, allProducts)
      const headsets = filterProducts(KeywordsToFilter.headsets, allProducts)
      const operatingSystemsWindows = filterProducts(KeywordsToFilter.operatingSystemsWindows, allProducts)
      const operatingSystemsUbuntu = filterProducts(KeywordsToFilter.operatingSystemsUbuntu, allProducts)
      const desks = filterProducts(KeywordsToFilter.desks, allProducts)

      const operatingSystems = [...operatingSystemsWindows, ...operatingSystemsUbuntu]

      dispatch({
        type: SomePiecesTypes.GET_PIECES_SUCCESS,
        payload: {allProducts, powerSources, motherBoards, processors, ssds, hdds, rams, keyboards, mouses, graphicsCards, liquidCoolers, screens, headsets, desks, operatingSystems}
      })
    } catch (error) {
      console.log(error);
      dispatch({
        type: SomePiecesTypes.GET_PIECES_ERROR,
        message: error.message
      })
    }
  }

  const setMotherBoardSelected = motherBoardObj => {
    dispatch({
      type: SomePiecesTypes.SET_MOTHER_BOARD_SELECTED,
      payload: motherBoardObj
    })
  }

  const setProcessorSelected = processorObj => {
    dispatch({
      type: SomePiecesTypes.SET_PROCESSOR_SELECTED,
      payload: processorObj
    })
  }
  
  const setRamSelected = ramObj => {
    dispatch({
      type: SomePiecesTypes.SET_RAM_SELECTED,
      payload: ramObj
    })
  }
  
  const setGraphicCardSelected = graphicCardObj => {
    dispatch({
      type: SomePiecesTypes.SET_GRAPHICS_CARD_SELECTED,
      payload: graphicCardObj
    })
  }
  
  const setLiquidCoolerSelected = liquidCoolerObj => {
    dispatch({
      type: SomePiecesTypes.SET_LIQUID_COOLER_SELECTED,
      payload: liquidCoolerObj
    })
  }
  
  const setPowerSourceSelected = powerSourceObj => {
    dispatch({
      type: SomePiecesTypes.SET_POWER_SOOURCE_SELECTED,
      payload: powerSourceObj
    })
  }
  
  const setHDDSelected = hddObj => {
    dispatch({
      type: SomePiecesTypes.SET_HDD_SELECTED,
      payload: hddObj
    })
  }
  
  const setSDDSelected = sddObj => {
    dispatch({
      type: SomePiecesTypes.SET_SSD_SELECTED,
      payload: sddObj
    })
  }
  
  const setKeyboardSelected = keyboardObj => {
    dispatch({
      type: SomePiecesTypes.SET_KEYBOARD_SELECTED,
      payload: keyboardObj
    })
  }
  
  const setMouseSelected = mouseObj => {
    dispatch({
      type: SomePiecesTypes.SET_MOUSE_SELECTED,
      payload: mouseObj
    })
  }
  
  const setScreenSelected = screenObj => {
    dispatch({
      type: SomePiecesTypes.SET_SCREEN_SELECTED,
      payload: screenObj
    })
  }
  
  const setHeadsetSelected = headsetObj => {
    dispatch({
      type: SomePiecesTypes.SET_HEADSET_SELECTED,
      payload: headsetObj
    })
  }
  
  const setOperatingSystemSelected = operatingSystemObj => {
    dispatch({
      type: SomePiecesTypes.SET_OPERATING_SYSTEM_SELECTED,
      payload: operatingSystemObj
    })
  }
  
  const setDeskSelected = deskObj => {
    dispatch({
      type: SomePiecesTypes.SET_DESK_SELECTED,
      payload: deskObj
    })
  }

  useEffect(() => {
    if(state.isFirstTime){
      getPieces()
    }
  }, [state.isFirstTime])

  return (
    <SomePiecesContext.Provider value={{
      pieces: state.pieces,
      isFetching: state.isFetching,
      error: state.error,
      message: state.message,
      powerSources: state.powerSources,
      motherBoards: state.motherBoards,
      processors: state.processors,
      ssds: state.ssds,
      hdds: state.hdds,
      rams: state.rams,
      keyboards: state.keyboards,
      mouses: state.mouses,
      graphicsCards: state.graphicsCards,
      liquidCoolers: state.liquidCoolers,
      screens: state.screens,
      headsets: state.headsets,
      operatingSystems: state.operatingSystems,
      desks: state.desks,
      isFetching: state.isFetching,
      isFirstTime: state.isFirstTime,
      error: state.error,
      message: state.message,
      setMotherBoardSelected,
      setProcessorSelected,
      setRamSelected,
      setGraphicCardSelected,
      setLiquidCoolerSelected,
      setPowerSourceSelected,
      setHDDSelected,
      setSDDSelected,
      setKeyboardSelected,
      setMouseSelected,
      setScreenSelected,
      setHeadsetSelected,
      setOperatingSystemSelected,
      setDeskSelected,
    }}>
      {children}
    </SomePiecesContext.Provider>
  )
}

// powerSourceSelected: {},
// motherBoardSelected: {},
// processorSelected: {},
// ssdSelected: {},
// hddSelected: {},
// ramSelected: {},
// keyboardSelected: {},
// mouseSelected: {},
// graphicsCardSelected: {},
// liquidCoolerSelected: {},
// screenSelected: {},
// headsetSelected: {},
// operatingSystemSelected: {},
// deskSelected: {},

// powerSourceSelected: state.powerSourceSelected,
// motherBoardSelected: state.motherBoardSelected,
// processorSelected: state.processorSelected,
// ssdSelected: state.ssdSelected,
// hddSelected: state.hddSelected,
// ramSelected: state.ramSelected,
// keyboardSelected: state.keyboardSelected,
// mouseSelected: state.mouseSelected,
// graphicsCardSelected: state.graphicsCardSelected,
// liquidCoolerSelected: state.liquidCoolerSelected,
// screenSelected: state.screenSelected,
// headsetSelected: state.headsetSelected,
// operatingSystemSelected: state.operatingSystemSelected,
// deskSelected: state.deskSelected,