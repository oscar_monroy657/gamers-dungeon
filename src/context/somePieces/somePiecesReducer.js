import { SomePiecesTypes } from "./somePiecesTypes";

const somePiecesReducer = (state, action) => {
  switch (action.type) {
    case SomePiecesTypes.GET_PIECES_START:
      return {
        ...state,
        isFetching: true,
        error: false,
        message: null,
      }
    case SomePiecesTypes.GET_PIECES_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isFirstTime: false,
        pieces: action.payload.allProducts,
        powerSources: action.payload.powerSources,
        motherBoards: action.payload.motherBoards,
        processors: action.payload.processors,
        ssds: action.payload.ssds,
        hdds: action.payload.hdds,
        rams: action.payload.rams,
        keyboards: action.payload.keyboards,
        mouses: action.payload.mouses,
        graphicsCards: action.payload.graphicsCards,
        liquidCoolers: action.payload.liquidCoolers,
        screens: action.payload.screens,
        headsets: action.payload.headsets,
        operatingSystems: action.payload.operatingSystems,
        desks: action.payload.desks,
      }
    case SomePiecesTypes.GET_PIECES_ERROR:
      return {
        ...state,
        isFetching: false,
        error: true,
        message: action.payload
      }
    case SomePiecesTypes.SET_MOTHER_BOARD_SELECTED:
      return {
        ...state,
        motherBoardSelected: action.payload
      }
    case SomePiecesTypes.SET_PROCESSOR_SELECTED:
      return {
        ...state,
        processorSelected: action.payload
      }
    case SomePiecesTypes.SET_RAM_SELECTED:
      return {
        ...state,
        ramSelected: action.payload
      }
    case SomePiecesTypes.SET_GRAPHICS_CARD_SELECTED:
      return {
        ...state,
        graphicsCardSelected: action.payload
      }
    case SomePiecesTypes.SET_LIQUID_COOLER_SELECTED:
      return {
        ...state,
        liquidCoolerSelected: action.payload
      }
    case SomePiecesTypes.SET_POWER_SOOURCE_SELECTED:
      return {
        ...state,
        powerSourceSelected: action.payload
      }
    case SomePiecesTypes.SET_HDD_SELECTED:
      return {
        ...state,
        hddSelected: action.payload
      }
    case SomePiecesTypes.SET_SSD_SELECTED:
      return {
        ...state,
        ssdSelected: action.payload
      }
    case SomePiecesTypes.SET_KEYBOARD_SELECTED:
      return {
        ...state,
        keyboardSelected: action.payload
      }
    case SomePiecesTypes.SET_MOUSE_SELECTED:
      return {
        ...state,
        mouseSelected: action.payload
      }
    case SomePiecesTypes.SET_SCREEN_SELECTED:
      return {
        ...state,
        screenSelected: action.payload
      }
    case SomePiecesTypes.SET_HEADSET_SELECTED:
      return {
        ...state,
        headsetSelected: action.payload
      }
    case SomePiecesTypes.SET_OPERATING_SYSTEM_SELECTED:
      return {
        ...state,
        operatingSystemSelected: action.payload
      }
    case SomePiecesTypes.SET_DESK_SELECTED:
      return {
        ...state,
        deskSelected: action.payload
      }
    default:
      return state
  }
}

export default somePiecesReducer