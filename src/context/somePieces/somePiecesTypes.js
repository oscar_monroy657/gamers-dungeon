export const SomePiecesTypes = {
  GET_PIECES_START: 'GET_PIECES_START',
  GET_PIECES_SUCCESS: 'GET_PIECES_SUCCESS',
  GET_PIECES_ERROR: 'GET_PIECES_ERROR',
  SET_POWER_SOOURCE_SELECTED: 'SET_POWER_SOOURCE_SELECTED',
  SET_MOTHER_BOARD_SELECTED: 'SET_MOTHER_BOARD_SELECTED',
  SET_PROCESSOR_SELECTED: 'SET_PROCESSOR_SELECTED',
  SET_SSD_SELECTED: 'SET_SSD_SELECTED',
  SET_HDD_SELECTED: 'SET_HDD_SELECTED',
  SET_RAM_SELECTED: 'SET_RAM_SELECTED',
  SET_KEYBOARD_SELECTED: 'SET_KEYBOARD_SELECTED',
  SET_MOUSE_SELECTED: 'SET_MOUSE_SELECTED',
  SET_GRAPHICS_CARD_SELECTED: 'SET_GRAPHICS_CARD_SELECTED',
  SET_LIQUID_COOLER_SELECTED: 'SET_LIQUID_COOLER_SELECTED',
  SET_SCREEN_SELECTED: 'SET_SCREEN_SELECTED',
  SET_HEADSET_SELECTED: 'SET_HEADSET_SELECTED',
  SET_OPERATING_SYSTEM_SELECTED: 'SET_OPERATING_SYSTEM_SELECTED',
  SET_DESK_SELECTED: 'SET_DESK_SELECTED',
}

// powerSourceSelected
// processorSelected
// ssdSelected
// hddSelected
// ramSelected
// keyboardSelected
// mouseSelected
// graphicsCardSelected
// liquidCoolerSelected
// screenSelected
// headsetSelected
// operatingSystemSelected
// deskSelected