export const filterProducts = (stringToEvaluate, allProductsArray) => {
  const products = allProductsArray
    .filter(product => product.nombre_producto.includes(stringToEvaluate))
    .sort((a, b) => a.precio_producto - b.precio_producto)

  return products
}