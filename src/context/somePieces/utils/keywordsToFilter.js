export const KeywordsToFilter = {
  powerSources: 'FUENTE DE PODER',
  motherBoards: 'MBOARD',
  processors: 'PROCESADOR',
  ssds: 'UNIDAD ESTADO SOLIDO',
  hdds: 'DISCO DURO INTERNO',
  rams: 'MEMORIA',
  keyboards: 'TECLADO',
  mouses: 'MOUSE',
  graphicsCards: 'VIDEO',
  liquidCoolers: 'LIQUID COOLER',
  screens: 'MONITOR',
  headsets: 'AUDIFONO',
  operatingSystemsWindows: 'WINDOWS',
  operatingSystemsUbuntu: 'UBUNTU',
  desks: 'PcDesk',
}

// combosDeTecladoYMouse: 'TECLADO' && 'MOUSE',
// KINGSTON HYPERX FURY RGB DDR4 16GB 2666MHZ CL16 DIMM HX426C16FB4A/16