import { useEffect, useReducer } from "react"
import axios from "axios";
import { createUserDocument, getCurrentUser, signInWithEmailAndPassword, signInWithGoogle, signOut, signupWithEmailAndPassword } from "../server/firebaseHelper";
import AppContext from "./AppContext";
import appReducer from "./appReducer"
import { AppTypes } from "./appTypes";
import { convertDataToSend } from "./helper/convertDataToSend";
// category: 'initial',

const AppState = ({ children }) => {
  const initialState = {
    category: 'complete_build',
    initialStepForBuildComplete: 0,
    operatingSystem: '',
    manufacturer: '',
    challenges: [],
    budget: {},
    budgetLimits: {},
    budgetToShow: [15000],
    accessories: [],
    products: null,
    user: null,
    error: false,
    isLoading: false,
    message: null,
    isChecking: true,
    isFetching: false,
    isFirstTime: true,
    showModal: false,
  }

  const [state, dispatch] = useReducer(appReducer, initialState);

  const setCategory = category => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_CATEGORY,
      payload: category
    })
  }
  
  const setOperatingSystem = name => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_OPERATING_SYSTEM,
      payload: name
    })
  }

  const setManufacturer = name => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_MANUFACTURER_NAME,
      payload: name
    })
  }

  const setChallenges = challenges => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_CHALLENGES,
      payload: challenges,
    })
  }

  const setBudget = (budget) => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_BUDGET,
      payload: budget
    })
  }

  const setBudgetLimits = (budgetLimits) => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_BUDGET_LIMITS,
      payload: budgetLimits
    })
  }
  const setBudgetToShow = (budgetToShow) => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_BUDGET_TO_SHOW,
      payload: budgetToShow
    })
  }

  const setAccessories = accessories => {
    dispatch({
      type: AppTypes.SET_ASSISTANT_ACCESSORIES,
      payload: accessories
    })
  }

  const getProducts = async (changeStep, accessories, showAlert) => {
    dispatch({
      type: AppTypes.GET_PRODUCTS_RESULT_START,
    })

    const dataToSendTemp = convertDataToSend(state, accessories)
    // console.log(dataToSendTemp)
    try {
      const firstResponse = await axios.post('https://gamersdungeon-api.herokuapp.com/products-api/v1/quiz/', dataToSendTemp)
      const quizId = firstResponse.data.quiz_id
      const productsResponse = await axios.post(`https://gamersdungeon-api.herokuapp.com/products-api/v1/quiz/answered/${quizId}`)

      const products = productsResponse.data.productos
      
      // const reduced = products.reduce((hash, row) => ({
      //     ...hash, [row.grupo]: {
      //       name: row.grupo,
      //       details: [...(hash[row.grupo]?.details ?? []), row]
      //     }
      // }), {})

      const reduced = products.reduce((hash, row) => {
        const stringIndex = row.grupo.indexOf('-') + 1
        const groupName = row.grupo.slice(stringIndex)
        
        return {
        ...hash, [groupName]: {
          name: groupName,
          details: [...(hash[row.grupo]?.details ?? []), row]
        }
    }}, {})

      const groupedProducts = Object.values(reduced).map(group => {
        // const total = group.details.reduce((total, row) => total + Number(row.precio), 0)
        const total = group.details[0].precio
        const product = group.details[0]
        const stringIndex = group.name.indexOf('-') + 1
        const groupName = group.name.slice(stringIndex)

        return {
          details: [product],
          name: groupName,
          total
        }
      })
      // showAlert()
      dispatch({
        type: AppTypes.GET_PRODUCTS_RESULT_SUCCESS,
        payload: groupedProducts
      })
      changeStep(6)
      window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
    } catch (error) {
      console.log(error);
      dispatch({
        type: AppTypes.GET_PRODUCTS_RESULT_FAILURE,
        payload: 'Ocurrió un error'
      })
    }
  }

  const setInitialStepForBuildComplete = (step) => {
    dispatch({
      type: AppTypes.SET_INITIAL_STEP_FOR_BUILD_COMPLETE,
      payload: step
    })
  }
  const hideModal = () => {
    dispatch({
      type: AppTypes.HIDE_MODAL
    })
  }

  // Auth Functions
  const isCheckingSessionSuccess = () => {
    dispatch({
      type: AppTypes.CHECKING_USER_AUTH_SUCCESS,
    })
  }

  const isCheckingSessionFailure = () => {
    dispatch({
      type: AppTypes.CHECKING_USER_AUTH_FAILURE,
    })
    cleanUserError()
  }

  const cleanUserError = () => {
    setTimeout(() => {
      dispatch({
        type: AppTypes.CLEAN_USER_ERROR
      })
    }, 5000);
  }

  const getSnapshotFromUser = async (userAuth, additionalData) => {
    try {
      const userRef = await createUserDocument(userAuth, additionalData )
      const userSnapshot = await userRef.get();
      const user = {id: userSnapshot?.id, ...userSnapshot?.data()}
      dispatch({
        type: AppTypes.GET_USER_DATA_SUCCESS,
        payload: user
      })
      isCheckingSessionSuccess()
    } catch (error) {
      dispatch({
        type: AppTypes.GET_USER_DATA_FAILURE,
        payload: error.message
      })
      isCheckingSessionFailure()
    }
  }

  const registerUser = async (email, password, additionalData) => {
    dispatch({
      type: AppTypes.REGISTER_USER_START
    })
    try {

      const { user } = await signupWithEmailAndPassword(email, password);
      await getSnapshotFromUser(user, additionalData)

    } catch (error) {
      dispatch({
        type: AppTypes.GET_USER_DATA_FAILURE,
        payload: error.message
      })
      cleanUserError()
    }
  }

  const signInUser = async (email, password) => {
    dispatch({
      type: AppTypes.SIGN_IN_USER_START
    })
    try {
      const { user } = await signInWithEmailAndPassword(email, password)
      await getSnapshotFromUser(user, null)
    } catch (error) {
      dispatch({
        type: AppTypes.GET_USER_DATA_FAILURE,
        payload: error.message
      })
      cleanUserError()
    }
  }

  const signInWithGoogleAccount = async () => {
    try {
      const { user } = await signInWithGoogle()
      await getSnapshotFromUser(user)
    } catch (error) {
      dispatch({
        type: AppTypes.GET_USER_DATA_FAILURE,
        payload: error.message
      })
      cleanUserError()
    }
  }

  const checkUserSession = async () => {
    dispatch({
      type: AppTypes.CHECKING_USER_AUTH_START,
    })
    try {
      const userAuth = await getCurrentUser()
      if(!userAuth){
        dispatch({
          type: AppTypes.CHECKING_USER_AUTH_FAILURE,
        })
        return
      }
      await getSnapshotFromUser(userAuth, null);
    } catch (error) {
      dispatch({
        type: AppTypes.GET_USER_DATA_FAILURE,
        payload: error.message
      })
      dispatch({
        type: AppTypes.CHECKING_USER_AUTH_FAILURE,
      })
      cleanUserError()
    }
  }

  const signOutApp = async () => {
    try {
      await signOut()
      dispatch({
        type: AppTypes.USER_SIGN_OUT_SUCCESS
      })
    } catch (error) {
      dispatch({
        type: AppTypes.USER_SIGN_OUT_FAILURE,
        payload: error.message
      })
    }
  }

  useEffect(() => {
    if(state.isFirstTime){
      checkUserSession()
      if(typeof window !== undefined){
        const infoFromLocalStorage = localStorage.getItem('showInstructions')
        if(infoFromLocalStorage){
          if(infoFromLocalStorage === 'No show'){
            // console.log(infoFromLocalStorage);
            setInitialStepForBuildComplete(1)
          }
        }
      }
    }
  }, [state.isFirstTime])

  return (
    <AppContext.Provider value={{
      category: state.category,
      initialStepForBuildComplete: state.initialStepForBuildComplete,
      challenges: state.challenges,
      operatingSystem: state.operatingSystem,
      manufacturer: state.manufacturer,
      accessories: state.accessories,
      products: state.products,
      budget: state.budget,
      budgetLimits: state.budgetLimits,
      budgetToShow: state.budgetToShow,
      user: state.user,
      error: state.error,
      isLoading: state.isLoading,
      message: state.message,
      isChecking: state.isChecking,
      isFetching: state.isFetching,
      showModal: state.showModal,
      setOperatingSystem,
      setManufacturer,
      setChallenges,
      setCategory,
      setBudget,
      setBudgetToShow,
      setBudgetLimits,
      setAccessories,
      registerUser,
      signInUser,
      signInWithGoogleAccount,
      signOutApp,
      checkUserSession,
      getProducts,
      setInitialStepForBuildComplete,
      hideModal
    }}>
      {children}
    </AppContext.Provider>
  )
}

export default AppState