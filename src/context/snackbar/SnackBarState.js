import { useReducer } from "react"
import SnackBarContext from "./SnackBarContext"
import snackBarReducer from "./snackBarReducer"
import { SnackBarTypes } from "./snackBarTypes"

export const SnackBarState = ({ children }) => {

  const initialState = {
    showSnackBar: false,
  }

  const [state, dispatch] = useReducer(snackBarReducer, initialState)

  const setShowSnackBar = (showSnackBar) => {
    if (showSnackBar) {
      return
    }
    dispatch({
      type: SnackBarTypes.SET_SHOW_SNACK_BAR
    })

    setTimeout(() => {
      dispatch({
        type: SnackBarTypes.SET_HIDE_SNACK_BAR
      })
    }, 3000);
  }

  const setHideSnackBar = () => {
    dispatch({
      type: SnackBarTypes.SET_HIDE_SNACK_BAR
    })
  }

  return (
    <SnackBarContext.Provider value={{
      showSnackBar: state.showSnackBar,
      setShowSnackBar,
      setHideSnackBar,
    }}>
      {children}
    </SnackBarContext.Provider>
  )
}
