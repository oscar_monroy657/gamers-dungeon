import { SnackBarTypes } from "./snackBarTypes";

const initialState = {
  showSnackBar: false,
}

const snackBarReducer = (state = initialState, action) => {
  switch (action.type) {
    case SnackBarTypes.SET_SHOW_SNACK_BAR:
      return {
        ...state,
        showSnackBar: true,
      }
    case SnackBarTypes.SET_HIDE_SNACK_BAR:
      return {
        ...state,
        showSnackBar: false,
      }
  
    default:
      return state
  }
}

export default snackBarReducer