export const dataToSendTemplate = {
  "id_categoria": 4,
  "paso": 1,
  "respuestas_paso": []
}

export const dataToSend = {
  "id_categoria": 4,
  "paso": 1,
  "respuestas_paso": [
    {
      "respuesta": "budgetMin",
      "nombre_visual_respuesta": "7000",
      "seleccionada": true
    },
    {
      "respuesta": "budgetMax",
      "nombre_visual_respuesta": "10000",
      "seleccionada": true
    },
    {
      "respuesta": "gaming",
      "nombre_visual_respuesta": "gaming",
      "seleccionada": true
    },
    {
      "respuesta": "videoconference",
      "nombre_visual_respuesta": "videoconference",
      "seleccionada": true
    }
  ]
}