import { ButtonToTopTypes } from "./buttonToTopTypes";

const buttonToTopReducer = (state, action) => {
  switch (action.type) {
    case ButtonToTopTypes.SET_WINDOW_SCROLL_POSITION:
      return {
        scrollPosition: action.payload,
      }
  
    default:
      return state
  }
}

export default buttonToTopReducer