import { useReducer } from "react"
import ButtonToTopContext from "./ButtonToTopContext"
import buttonToTopReducer from "./buttonToTopReducer"
import { ButtonToTopTypes } from "./buttonToTopTypes"

const ButtonToTopState = ({ children }) => {
  const initialState = {
    scrollPosition: 0
  }

  const [state, dispatch] = useReducer(buttonToTopReducer, initialState)

  const setScrollPosition = scrollValue => {
    dispatch({
      type: ButtonToTopTypes.SET_WINDOW_SCROLL_POSITION,
      payload: scrollValue
    })
  }

  return (
    <ButtonToTopContext.Provider value={{
      scrollPosition: state.scrollPosition,
      setScrollPosition,
    }}>
      {children}
    </ButtonToTopContext.Provider>
  )
}

export default ButtonToTopState