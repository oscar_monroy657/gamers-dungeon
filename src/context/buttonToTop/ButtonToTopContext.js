import { createContext } from 'react'

const ButtonToTopContext = createContext()

export default ButtonToTopContext