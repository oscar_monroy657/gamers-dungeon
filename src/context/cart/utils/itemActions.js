export const addItemToCart = (cartItems, cartItemToAdd) => {
	const existingCartItem = cartItems.find((cartItem) => cartItem.id_producto === cartItemToAdd.id_producto);

	if (existingCartItem) {
		return cartItems.map((cartItem) =>
			cartItem.id_producto === existingCartItem.id_producto
				? { ...cartItem, quantity: cartItem.quantity + 1 }
				: cartItem
		);
	}

	return [...cartItems, { ...cartItemToAdd, quantity: 1 }];
};

export const removeItemFromCart = (cartItems, cartItemToRemove) => {
	const existingCartItem = cartItems.find(
		(cartItem) => cartItem.id_producto === cartItemToRemove.id_producto
	);

	if (existingCartItem.quantity === 1) {
		return cartItems.filter((cartItem) => cartItem.id_producto !== cartItemToRemove.id_producto);
	}

	return cartItems.map((cartItem) =>
		cartItem.id_producto === cartItemToRemove.id_producto
			? { ...cartItem, quantity: cartItem.quantity - 1 }
			: cartItem
	);
};

export const clearItemFromCart = (cartItems, cartItemToDelete) => {
  const newCartItems = cartItems.filter(item => item.id_producto !== cartItemToDelete.id_producto)

  return newCartItems
}

export const getCountItems = (arrayItems) => {
  const countItems = arrayItems.reduce((count, item) => (count ?? 0) + item.quantity, 0)

  return countItems
}

export const getTotalToPay = (arrayItems) => {
  const totalToPay = arrayItems.reduce((total, item) => (item.quantity * (item.precio_producto || item.precio)) + total, 0)

  return parseFloat(totalToPay)
}
