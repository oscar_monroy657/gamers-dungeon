export const setCartToLocalStorage = cartArray => {
  const cartForLocalStorage = JSON.stringify(cartArray)
  localStorage.setItem('dgCart', cartForLocalStorage)
}

export const getCartFromLocalStorage = () => {
  const cartFromLocalStorage = localStorage.getItem('dgCart')
  if(cartFromLocalStorage){
    const cartArray = JSON.parse(cartFromLocalStorage)
    return [...cartArray]
  }
  return []
}

export const deleteCartFromLocalStorage = () => {
  localStorage.removeItem('dgCart')
}