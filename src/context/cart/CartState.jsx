import { useEffect, useReducer } from "react"
import CartContext from "./CartContext"
import cartReducer from "./cartReducer"
import { CartTypes } from "./cartTypes"
import { addItemToCart, clearItemFromCart, getCountItems, getTotalToPay, removeItemFromCart } from "./utils/itemActions"
import { deleteCartFromLocalStorage, getCartFromLocalStorage, setCartToLocalStorage } from "./utils/persistentCart"

const CartState = ({ children }) => {
  const initialState = {
    hidde: true,
    cartItems: [],
    cartItemsCount: 0,
    totalToPay: 0,
    isFirstTime: true,
    totalForPaypal: 0,
    invoice: null,
  }

  const [state, dispatch] = useReducer(cartReducer, initialState)

  const setCountItemsAndTotalToPay = (itemsArray) => {
    const countItems = getCountItems(itemsArray)
    const totalToPay = getTotalToPay(itemsArray)

    setCartToLocalStorage(itemsArray)

    dispatch({
      type: CartTypes.SET_COUNT_AND_TOTAL_TO_PAY,
      payload: { countItems, totalToPay }
    })
  }

  const addComboToCart = cartItemsArray => {
    dispatch({
      type: CartTypes.ADD_COMBO_TO_CART,
      payload: cartItemsArray
    })
    setCountItemsAndTotalToPay(cartItemsArray)
  }

  const addItem = (itemToAdd, cartItems) => {
    const itemsArray = addItemToCart(cartItems, itemToAdd)
    dispatch({
      type: CartTypes.ADD_ITEM,
      payload: itemsArray
    })
    setCountItemsAndTotalToPay(itemsArray)
  }

  const removeItem = (itemToRemove, cartItems) => {
    const itemsArray = removeItemFromCart(cartItems, itemToRemove)
    dispatch({
      type: CartTypes.REMOVE_ITEM,
      payload: itemsArray
    })
    setCountItemsAndTotalToPay(itemsArray)
  }

  const setTotalForPaypal = total => {
    dispatch({
      type: CartTypes.SET_TOTAL_FOR_PAYPAL,
      payload: total
    })
  }

  const cleanItemFromCart = (itemToClear, cartItems) => {
    const itemsArray = clearItemFromCart(cartItems, itemToClear)
    dispatch({
      type: CartTypes.CLEAR_ITEM_FROM_CART,
      payload: itemsArray
    })
    setCountItemsAndTotalToPay(itemsArray)
  }

  const setInvoiceData = invoice => {
    dispatch({
      type: CartTypes.SET_INVOICE_DATA,
      payload: invoice
    })
  }

  const clearCart = () => {
    deleteCartFromLocalStorage()
    dispatch({
      type: CartTypes.CLEAR_CART
    })
  }

  useEffect(() => {
    if(typeof window !== undefined){
      if(state.isFirstTime){
        const cartItems = getCartFromLocalStorage()
        addComboToCart(cartItems)
      }
    }
  }, [])

  return (
    <CartContext.Provider value={{
      hidde: state.hidde,
      cartItems: state.cartItems,
      cartItemsCount: state.cartItemsCount,
      totalToPay: state.totalToPay,
      totalForPaypal: state.totalForPaypal,
      invoice: state.invoice,
      addComboToCart,
      addItem,
      removeItem,
      setTotalForPaypal,
      cleanItemFromCart,
      setInvoiceData,
      clearCart
    }}>
      {children}
    </CartContext.Provider>
  )
}

export default CartState