import { CartTypes } from "./cartTypes";

const cartReducer = ( state, action ) => {
  switch (action.type) {
    case CartTypes.TOGGLE_CART_HIDDEN:
      return {
        ...state,
        hidden: !state.hidden
      }
    case CartTypes.ADD_COMBO_TO_CART: 
      return {
        ...state,
        cartItems: action.payload,
        isFirstTime: false,
      }
    case CartTypes.ADD_ITEM:
    case CartTypes.REMOVE_ITEM:
    case CartTypes.CLEAR_ITEM_FROM_CART:
      return {
        ...state,
        cartItems: action.payload,
        isFirstTime: false,
      }
    case CartTypes.SET_COUNT_AND_TOTAL_TO_PAY:
      return {
        ...state,
        cartItemsCount: action.payload.countItems,
        totalToPay: action.payload.totalToPay,
      }
    case CartTypes.SET_TOTAL_FOR_PAYPAL:
      return {
        ...state,
        totalForPaypal: action.payload
      }
    case CartTypes.SET_INVOICE_DATA:
      return {
        ...state,
        invoice: action.payload
      }
    case CartTypes.CLEAR_CART:
      return {
        ...state,
        cartItems: [],
        cartItemsCount: 0,
        totalToPay: 0,
        isFirstTime: false,
        totalForPaypal: 0,
      }
    default:
      return state
  }
}

export default cartReducer