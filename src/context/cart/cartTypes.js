export const CartTypes = {
  TOGGLE_CART_HIDDEN: 'TOGGLE_CART_HIDDEN',
  ADD_COMBO_TO_CART: 'ADD_COMBO_TO_CART',
  ADD_ITEM: 'ADD_ITEM',
  SET_COUNT_AND_TOTAL_TO_PAY: 'SET_COUNT_AND_TOTAL_TO_PAY',
  SET_TOTAL_FOR_PAYPAL: 'SET_TOTAL_FOR_PAYPAL',
  REMOVE_ITEM: 'REMOVE_ITEM',
  CLEAR_ITEM_FROM_CART: 'CLEAR_ITEM_FROM_CART',
  SET_INVOICE_DATA: 'SET_INVOICE_DATA',
  CLEAR_CART: 'CLEAR_CART'
}