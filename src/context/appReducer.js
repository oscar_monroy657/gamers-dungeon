import { AppTypes } from "./appTypes";

const initialState = {
  category: 'initial',
  initialStepForBuildComplete: 0,
  operatingSystem: '',
  manufacturer: '',
  challenges: [],
  budget: {},
  budgetLimits: {},
  budgetToShow: [15000],
  accessories: [],
  products: null,
  user: null,
  error: false,
  isLoading: false,
  message: null,
  isChecking: true,
  isFetching: false,
  isFirstTime: true,
  showModal: false,
}

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case AppTypes.SET_ASSISTANT_CATEGORY:
      return {
        ...state,
        category: action.payload,
      }
    case AppTypes.SET_INITIAL_STEP_FOR_BUILD_COMPLETE:
      return {
        ...state,
        initialStepForBuildComplete: action.payload
      }
    case AppTypes.SET_ASSISTANT_OPERATING_SYSTEM:
      return {
        ...state,
        operatingSystem: action.payload
      }
    case AppTypes.SET_ASSISTANT_MANUFACTURER_NAME:
      return {
        ...state,
        manufacturer: action.payload
      }
    case AppTypes.SET_ASSISTANT_CHALLENGES:
      return {
        ...state,
        challenges: action.payload
      }
    case AppTypes.SET_ASSISTANT_BUDGET:
      return {
        ...state,
        budget: action.payload,
      }
    case AppTypes.SET_ASSISTANT_BUDGET_TO_SHOW:
      return {
        ...state,
        budgetToShow: action.payload,
      }
    case AppTypes.SET_ASSISTANT_BUDGET_LIMITS:
      return {
        ...state,
        budgetLimits: action.payload
      }
    case AppTypes.SET_ASSISTANT_ACCESSORIES:
      return {
        ...state,
        accessories: action.payload
      }
    case AppTypes.HIDE_MODAL:
      return {
        ...state,
        showModal: false,
      }
      case AppTypes.CHECKING_USER_AUTH_START:
        return {
          ...state,
          isChecking: true,
        }
      case AppTypes.CHECKING_USER_AUTH_SUCCESS:
      case AppTypes.CHECKING_USER_AUTH_FAILURE:
        return {
          ...state,
          isChecking: false,
        }
      case AppTypes.REGISTER_USER_START:
      case AppTypes.SIGN_IN_USER_START:
        return {
          ...state,
          isLoading: true,
          error: false,
          message: null,
        }
      case AppTypes.GET_USER_DATA_SUCCESS:
        return {
          ...state,
          user: action.payload,
          isLoading: false,
          error: false,
          isFirstTime: false,
        }
      case AppTypes.GET_USER_DATA_FAILURE:
        return {
          ...state,
          user: null,
          isLoading: false,
          error: true,
          message: action.payload,
        }
      case AppTypes.USER_SIGN_OUT_SUCCESS:
        return {
          ...state,
          category: 'initial',
          challenges: [],
          budget: {},
          accessories: [],
          user: null,
        }
      case AppTypes.USER_SIGN_OUT_FAILURE:
        return {
          ...state,
          message: action.payload
        }
      case AppTypes.GET_PRODUCTS_RESULT_START:
        return {
          ...state,
          isFetching: true,
          error: false,
          message: null,
        }
      case AppTypes.GET_PRODUCTS_RESULT_SUCCESS:
        return {
          ...state,
          isFetching: false,
          products: action.payload,
          showModal: true
        }
      case AppTypes.GET_PRODUCTS_RESULT_FAILURE:
        return {
          ...state,
          isFetching: false,
          error: true,
          message: action.payload
        }
      case AppTypes.CLEAN_USER_ERROR:
        return {
          ...state,
          error: false,
          message: null,
        }
    default:
      return state
  }
}

export default appReducer