import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import axios from "axios";

const firebaseConfig = {
  apiKey: "AIzaSyALBP6C7eYzzZrMJZc_nbiBZBL8uUZX2zg",
  authDomain: "gamers-dungeon.firebaseapp.com",
  databaseURL: "https://gamers-dungeon.firebaseio.com",
  projectId: "gamers-dungeon",
  storageBucket: "gamers-dungeon.appspot.com",
  messagingSenderId: "1049649458765",
  appId: "1:1049649458765:web:c20e0b0a183508c70ef604",
  measurementId: "G-69J5GQ5YMV"
};

if(!firebase.apps.length){
  firebase.initializeApp(firebaseConfig)
}else{
  firebase.app()
}

const firestore = firebase.firestore()
const auth = firebase.auth()

export const signupWithEmailAndPassword = (email, password) => {
  return auth.createUserWithEmailAndPassword(email, password)
}

export const signInWithEmailAndPassword = (email, password) => {
  return auth.signInWithEmailAndPassword(email, password)
}

export const signOut = () => {
  return auth.signOut()
}

const getIdForInvoice = async ( name, email ) => {
  const userForApi = {
    "user_full_name": 'Test',
    "user_email": email,
    "user_password": "Test1345!",
    "user_password_confirmation":"Test1345!"
  }

  const userDataFromApi = await axios.post('https://gamersdungeon-api.herokuapp.com/products-api/v1/users/register', userForApi)
  await axios.post('https://gamersdungeon-api.herokuapp.com/products-api/v1/mail/after-register', {
    "email_to": email
  })
  
  const user_id = userDataFromApi.data.id_cliente

  return user_id
}

export const createUserDocument = async (userAuth, additionalData) => {
  if(!userAuth) return;

  const userRef = firestore.doc(`Users/${userAuth.uid}`);
  const userSnapshot = await userRef.get();

  if(!userSnapshot.exists){
    const { displayName, email } = userAuth;
    const userCreatedAt = new Date();

    try {
      const userId = await getIdForInvoice(displayName, email)

      await userRef.set({
        displayName,
        email,
        userCreatedAt,
        userId,
        ...additionalData
      })
    } catch (error) {
      console.log('Error creating user', error.message);
    }
  }

  return userRef;
}

export const getCurrentUser = () => {
  return new Promise((resolve, reject) => {
    const unsuscribe = auth.onAuthStateChanged(userAuth => {
      unsuscribe();
      resolve(userAuth)
    }, reject)
  })
}

export const signInWithGoogle = () => {
  const googleProvider = new firebase.auth.GoogleAuthProvider()
  googleProvider.setCustomParameters({ prompt: 'select_account'})

  return auth.signInWithPopup(googleProvider)
}