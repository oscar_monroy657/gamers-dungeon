import React, { useContext } from 'react'
import { options } from '../../../utils/constants'
import CartContext from '../../context/cart/CartContext'
import SnackBarContext from '../../context/snackbar/SnackBarContext'
import { NextButton } from '../assistent/assistentStyles'
import { Item, ItemImage, ItemTitle  } from '../listOfProducts/productItemStyles'
import * as gtag from '../../../utils/gtag'

export const ProductItem = ({ item }) => {
  const { cartItems, addItem } = useContext(CartContext)
  const { setShowSnackBar, showSnackBar } = useContext(SnackBarContext)

  const handleClick = () => {
    import('react-facebook-pixel')
      .then(module => module.default)
      .then(ReactPixel => {
        ReactPixel.init('531836734572627', null, options)
        ReactPixel.pageView()
        ReactPixel.track('AddToCart', { productId: item.id_producto, price: item.precio_producto })
        console.log('Add cart');
      })
    gtag.event({
      action: 'add_to_cart',
      category: 'ecommerce',
      label: 'Item added',
      value: item.nombre_producto
    })
    setShowSnackBar(showSnackBar)
    addItem(item, cartItems)
  }

  return (
    <Item>
      <ItemImage>
        <img src={item.img_url} alt={item.nombre_producto} />
      </ItemImage>
      <ItemTitle>{item.nombre_producto}</ItemTitle>
      <p>
				Precio:{' '}
				<strong>
					Q
					{Number(item.precio_producto ?? 0)
						.toFixed(2)
						.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
				</strong>
			</p>
      <NextButton onClick={handleClick}>Añadir al carrito</NextButton>
    </Item>
  )
}
