import { VideoContainer, VideoResource, VideoSectionContainer, VideoSectionDescription, VideoSectionPlayer } from "./homeVideoSectionStyles"

const HomeVideoSection = () => {
  return (
    <VideoSectionContainer>
      <VideoSectionDescription>
        <h2>¡Tenemos los mejores escritorios!</h2>
        <p>Personalizamos el escritorio a tus necesidades y gustos. ¡Tú tienes el control!</p>
      </VideoSectionDescription>
      <VideoSectionPlayer>
        <VideoContainer>
          <VideoResource
            width={853}
            height={480}
            src={'https://www.youtube.com/embed/pjuD4wKX8u0'}
            frameBorder={0}
            allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
            allowFullScreen
            title='Escritorios'
          />
        </VideoContainer>
      </VideoSectionPlayer>
    </VideoSectionContainer>
  )
}

export default HomeVideoSection