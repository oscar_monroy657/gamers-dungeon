import styled from "styled-components";
import device from "../../utils/css/breakpoints";

export const VideoSectionContainer = styled.section`
	display: flex;
	flex-direction: column-reverse;
	width: 100%;

	@media ${device.tablet} {
		flex-direction: row;
	}
`;

export const VideoSectionDescription = styled.div`
	width: 95%;
	padding: 2rem;
	box-shadow: 0 3px 11px -5px rgba(0 0 0 / 30%);
	margin: 0 auto;

  & h2 {
    font-size: 2.5rem;
  }
  
	@media ${device.tablet} {
    width: 50%;
    margin: 2rem;
    padding: 3rem;

    & h2 {
      font-size: 2.8rem;
    }
	}
`;

export const VideoSectionPlayer = styled.div`
	width: 100%;

	@media ${device.tablet} {
		width: 50%;
	}
`;
export const VideoContainer = styled.div`
	padding-bottom: 56.25%;
	position: relative;
	overflow: hidden;
	height: 0;
`;
export const VideoResource = styled.iframe`
	position: absolute;
	top: 0;
	left: 0;
	height: 100%;
  width: 100%;
	/* right: 0;
	bottom: 0; */
`;
