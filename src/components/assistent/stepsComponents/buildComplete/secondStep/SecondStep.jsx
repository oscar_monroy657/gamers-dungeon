import { useRouter } from 'next/router';
import { useContext, useEffect } from 'react';
import AppContext from '../../../../../context/AppContext';
import { ButtonContainer, NextButton, OptionIconContainer, OptionLabelContainer, OptionsContainer } from '../../../assistentStyles';
import { robotMessages } from '../../../../../utils/robotMessages/robotMessages';
import { RobotMessage } from '../../../../robotMessage/RobotMessage';
import { RobotContainer, RobotIconContainer } from '../../../../robotMessage/robotMessageStyles';
import { useModal } from '../../../../../hooks/useModal';

export const SecondStep = ({ changeStep }) => {
	const { user, manufacturer, setManufacturer, setBudgetLimits } = useContext(AppContext);
	const router = useRouter();
	const [ isOpen, openModal, closeModal ] = useModal(false)
	const urlForAssistant = "https://www.google.com"
	const messageForAssintant = robotMessages.manufacturer.message()

	useEffect(() => {
		if (!user) {
			router.replace('/login');
		}
	}, [user]);

	const handleOnClick = (option) => {
		if(option === 'intel'){
			setBudgetLimits({min: 10000, max: 64000})
		}else {
			setBudgetLimits({min: 11000, max: 64000})
		}
		setManufacturer(option)
		setTimeout(() => {
			window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
			changeStep(3);
		}, 200);
	};
	const handlePreviousStep = () => {
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth' });
		changeStep(1);
	};

	return (
		<>
			<RobotMessage isOpen={isOpen} closeModal={closeModal} url={urlForAssistant} message={messageForAssintant}/>
			<RobotContainer>
				<h2>Elije el fabricante</h2>
				<RobotIconContainer onClick={openModal} ml>
					<img src="/assets/bot.svg" alt="asistente online" />
				</RobotIconContainer>
			</RobotContainer>
			<OptionsContainer>
				<OptionLabelContainer onClick={() => handleOnClick('intel')}>
					<h3>Intel</h3>
					<OptionIconContainer>
						<img src='/assets/intel.svg' alt='manufacturer intel option' />
					</OptionIconContainer>
					<input
						type='radio'
						name='quiz-option'
            defaultChecked={manufacturer === 'intel'}
					/>
				</OptionLabelContainer>
				<OptionLabelContainer onClick={() => handleOnClick('amd')}>
					<h3>AMD</h3>
					<OptionIconContainer>
						<img src='/assets/amd.svg' alt='manufacturer amd option' />
					</OptionIconContainer>
					<input
						type='radio'
						name='quiz-option'
            defaultChecked={manufacturer === 'amd'}
					/>
				</OptionLabelContainer>
			</OptionsContainer>
			<ButtonContainer>
				<NextButton onClick={handlePreviousStep}>Anterior</NextButton>
				{/* <NextButton disabled={isDisabled} onClick={handleOnClick}>Siguiente</NextButton> */}
			</ButtonContainer>
		</>
	);
};
