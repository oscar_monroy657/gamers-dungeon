import { useRouter } from 'next/router'
import { useContext, useState } from 'react'
import Swal from 'sweetalert2'
import AppContext from '../../../../../context/AppContext'
import CartContext from '../../../../../context/cart/CartContext'
import { addItemToCart } from '../../../../../context/cart/utils/itemActions'
import { useScrollPosition } from '../../../../../hooks/useScrollPosition'
import { ButtonToTop } from '../../../../buttonToTop/ButtonToTop'
import { ListOfProducts } from '../../../../listOfProducts/ListOfProducts'
import { ProductsTitle, ProductSubtitle } from '../../../../listOfProducts/listProductsStyles'
import { ButtonContainer, NextButton } from '../../../assistentStyles'
import { useModal } from '../../../../../hooks/useModal';
import { robotMessages } from '../../../../../utils/robotMessages/robotMessages';
import { RobotMessage } from '../../../../robotMessage/RobotMessage';
import { RobotContainer, RobotIconContainer } from '../../../../robotMessage/robotMessageStyles';
import * as gtag from '../../../../../../utils/gtag'

export const SixthStep = ({changeStep}) => {
  const { products, setChallenges, setAccessories, setManufacturer, setOperatingSystem, hideModal } = useContext(AppContext)
  const { cartItems, addComboToCart } = useContext(CartContext)
  const [hasBeenAddedToCart, setHasBeenAddedToCart] = useState(false)
  const scrollPosition = useScrollPosition();
  const router = useRouter()
  const [ isOpen, openModal, closeModal ] = useModal(true)
	const urlForAssistant = "https://www.google.com"
	const messageForAssintant = robotMessages.finalStep.message()

  const resetQuiz = () => {
    gtag.event({
      action: 'assistant_rejected',
      category: 'assistant_result',
      label: 'Assistant rejected',
      value: 'Results rejected'
    })
    changeStep(1)
    setChallenges([])
    setAccessories([])
    setManufacturer('')
    setOperatingSystem('')
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
  }

  const addToCart = () => {
    const productsDetails = products.map(product => product.details)
    const productsToAdd = productsDetails
    let _cartItems = cartItems
    let newCartItems

    productsToAdd.forEach(productToAdd => {
      newCartItems = addItemToCart(_cartItems, {...productToAdd[0]})
      _cartItems = newCartItems
    })
    addComboToCart(newCartItems)
    setHasBeenAddedToCart(true)
    gtag.event({
      action: 'assistant_success',
      category: 'assistant_result',
      label: 'Assistant success',
      value: 'Results accepted'
    })
    Swal.fire('Agregado al carrito', 'Tus productos se han agregado satisfactoriamente.', 'success')
  }

  const goToCart = () => {
    setChallenges([])
    setAccessories([])
    setManufacturer('')
    setOperatingSystem('')
    router.push('/checkout')
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
  }

  return (
    <>
      <RobotMessage isOpen={isOpen} closeModal={closeModal} url={urlForAssistant} message={messageForAssintant} noShowLink={true}/>
      <RobotContainer>
        <ProductsTitle>Productos</ProductsTitle>
				<RobotIconContainer onClick={openModal} ml>
					<img src="/assets/bot.svg" alt="asistente online" />
				</RobotIconContainer>
			</RobotContainer>
      <ProductSubtitle>Estos productos se ajustan a tus elecciones</ProductSubtitle>
      <div>
        {products.length > 0 ? (
          <ListOfProducts products={products}/>
        ): (
          <p>No hay productos para mostrar</p>
        )}
      </div>
      <ButtonContainer>
        <NextButton onClick={resetQuiz}>Realizar otro quiz</NextButton>
        {!hasBeenAddedToCart && <NextButton onClick={addToCart}>Añadir al carrito</NextButton>}
        {hasBeenAddedToCart && <NextButton onClick={goToCart}>Ir al carrito</NextButton>}
      </ButtonContainer>
      {scrollPosition > 500 && <ButtonToTop/>}
    </>
  )
}
