import { useContext, useState, useEffect } from "react";
import AppContext from "../../../../../context/AppContext";
import { OptionCheckboxContainer, OptionIconContainer, OptionsContainer, NextButton, OptionLabel, ButtonContainer, } from "../../../assistentStyles";
import { useModal } from '../../../../../hooks/useModal';
import { robotMessages } from '../../../../../utils/robotMessages/robotMessages';
import { RobotMessage } from '../../../../robotMessage/RobotMessage';
import { RobotContainer, RobotIconContainer } from '../../../../robotMessage/robotMessageStyles';

export const ThirdStep = ({ changeStep }) => {
	const [listOfChallenges, setListOfChallenges] = useState([]);
	const { challenges, setChallenges } = useContext(AppContext);
	const [ isOpen, openModal, closeModal ] = useModal(false)
	const urlForAssistant = "https://www.google.com"
	const messageForAssintant = robotMessages.challenges.message()

  useEffect(() => {
    setListOfChallenges(challenges)
  }, [])

	const handleOnChange = (e) => {
		const { value } = e.target;
		if (listOfChallenges.includes(value)) {
			const newListOfChanllenges = listOfChallenges.filter(
				(challenge) => challenge !== value
			);
			setListOfChallenges(newListOfChanllenges);
			setChallenges(newListOfChanllenges)
		} else {
			setListOfChallenges([...listOfChallenges, value]);
			setChallenges([...listOfChallenges, value]);
		}
	};

	const handleOnClick = () => {
		setChallenges(listOfChallenges);
		window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
		changeStep(4);
	};

	const handlePreviousStep = () => {
		window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
		changeStep(2)
	}

	return (
		<>
			<RobotMessage isOpen={isOpen} closeModal={closeModal} url={urlForAssistant} message={messageForAssintant}/>
			<RobotContainer>
				<h2>¿Cuáles son tus desafíos?</h2>
				<RobotIconContainer onClick={openModal} ml>
					<img src="/assets/bot.svg" alt="asistente online" />
				</RobotIconContainer>
			</RobotContainer>
			<OptionsContainer>
				<OptionCheckboxContainer>
					<h3>Básico</h3>
					<OptionIconContainer>
						<img src="/assets/basic.svg" alt="desk option" />
					</OptionIconContainer>
					<OptionLabel>
						<input
							type="checkbox"
							name="office"
							value="office"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'office')}
						/>{" "}
						Oficina
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="web_navigation"
							value="web_navigation"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'web_navigation')}
						/>{" "}
						Navegar
					</OptionLabel>
					<OptionLabel noBorder>
						<input
							type="checkbox"
							name="videoconference"
							value="videoconference"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'videoconference')}
						/>{" "}
						Videoconferencias
					</OptionLabel>
				</OptionCheckboxContainer>
				<OptionCheckboxContainer>
					<h3>Videojuegos</h3>
					<OptionIconContainer>
						<img src="/assets/gaming.svg" alt="desk option" />
					</OptionIconContainer>
					<OptionLabel>
						<input
							type="checkbox"
							name="gaming"
							value="gaming"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'gaming')}
						/>{" "}
						Gaming
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="live_stream"
							value="live_stream"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'live_stream')}
						/>{" "}
						Live stream
					</OptionLabel>
					<OptionLabel noBorder>
						<input
							type="checkbox"
							name="virtual_reality"
							value="virtual_reality"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'virtual_reality')}
						/>{" "}
						Realidad virtual
					</OptionLabel>
				</OptionCheckboxContainer>
				{/* <OptionCheckboxContainer>
					<h3>Renders y modelado 3D</h3>
					<OptionIconContainer>
						<img src="/assets/3d-modeling.svg" alt="some pieces option" />
					</OptionIconContainer>
					<OptionLabel>
						<input
							type="checkbox"
							name="architecture"
							value="architecture"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'architecture')}
						/>{" "}
						Arquitectura
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="engineering"
							value="engineering"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'engineering')}
						/>{" "}
						Ingeniería
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="virtual_and_augmented_reality"
							value="virtual_and_augmented_reality"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'virtual_and_augmented_reality')}
						/>{" "}
						Realidad virtual y aumentada
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="industrial_design"
							value="industrial_design"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'industrial_design')}
						/>{" "}
						Diseño industrial
					</OptionLabel>
					<OptionLabel noBorder>
						<input
							type="checkbox"
							name="2D_drawings"
							value="2D_drawings"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === '2D_drawings')}
						/>{" "}
						Planos 2D
					</OptionLabel>
				</OptionCheckboxContainer> */}
				<OptionCheckboxContainer>
					<h3>Programación y sistemas</h3>
					<OptionIconContainer>
						<img src="/assets/coding.svg" alt="some pieces option" />
					</OptionIconContainer>
					<OptionLabel>
						<input
							type="checkbox"
							name="software_development"
							value="software_development"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'software_development')}
						/>{" "}
						Desarrollo de software
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="simulations_(mathematical/scientific)"
							value="simulations_(mathematical/scientific)"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'simulations_(mathematical/scientific)')}
						/> Simulaciones (mátematicas / científicas)
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="data_science"
							value="data_science"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'data_science')}
						/>{" "}
						Data science
					</OptionLabel>
					<OptionLabel noBorder>
						<input
							type="checkbox"
							name="virtual_machines"
							value="virtual_machines"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'virtual_machines')}
						/>{" "}
						Maquinas virtuales
					</OptionLabel>
				</OptionCheckboxContainer>
				<OptionCheckboxContainer>
					<h3>Edición y diseño</h3>
					<OptionIconContainer>
						<img
							src="/assets/design.svg"
							alt="complete option"
						/>
					</OptionIconContainer>
					<OptionLabel>
						<input
							type="checkbox"
							name="video_edition"
							value="video_edition"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'video_edition')}
						/>{" "}
						Edición de video
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="photography"
							value="photography"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'photography')}
						/>{" "}
						Fotografía
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="animation"
							value="animation"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'animation')}
						/>{" "}
						Animación digital
					</OptionLabel>
					<OptionLabel>
						<input
							type="checkbox"
							name="audio_production"
							value="audio_production"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'audio_production')}
						/>{" "}
						Producción de audio
					</OptionLabel>
					<OptionLabel noBorder>
						<input
							type="checkbox"
							name="graphic_design"
							value="graphic_design"
							onChange={handleOnChange}
              checked={listOfChallenges.some(item => item === 'graphic_design')}
						/>{" "}
						Diseño gráfico
					</OptionLabel>
				</OptionCheckboxContainer>
			</OptionsContainer>
			<ButtonContainer>
				<NextButton onClick={handlePreviousStep}>Anterior</NextButton>
				<NextButton
					disabled={listOfChallenges.length > 0 ? false : true}
					onClick={handleOnClick}
				>
					Siguiente
				</NextButton>
			</ButtonContainer>
		</>
	);
};
