import { useState, useContext } from "react";
import AppContext from "../../../../../context/AppContext";
import { NextButton, ButtonContainer } from '../../../assistentStyles'
import { BadgetRange } from '../../../../badgetRange/BadgetRange'
import { useEffect } from "react";
import { getBudgetToSend } from "./util/selectBudget";
import { useModal } from '../../../../../hooks/useModal';
import { robotMessages } from '../../../../../utils/robotMessages/robotMessages';
import { RobotMessage } from '../../../../robotMessage/RobotMessage';
import { RobotContainer, RobotIconContainer } from '../../../../robotMessage/robotMessageStyles';

export const FourthStep = ({ changeStep }) => {
  const [valuesToSend, setValuesToSend] = useState([])
  const [changeValuesToSend, setChangeValuesToSend] = useState(false)
  const { setBudget, manufacturer, budgetToShow } = useContext(AppContext);
  const [ isOpen, openModal, closeModal ] = useModal(false)
	const urlForAssistant = "https://www.google.com"
	const messageForAssintant = robotMessages.bullet.message()

  useEffect(() => {
    setValuesToSend(budgetToShow)
  }, [])

  useEffect(() => {
    if(changeValuesToSend){
      setValuesToSend(budgetToShow)
      setChangeValuesToSend(false)
    }
  }, [changeValuesToSend])

  const handleNextStep = () => {
    // console.log(valuesToSend[0].toString())
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
    const budgetToSend = getBudgetToSend(valuesToSend[0], manufacturer)
    // console.log(valuesToSend[0], budgetToSend);
    setBudget(budgetToSend);
    changeStep(5)
  }

  const handlePreviousStep = () => {
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
    changeStep(3)
  }

  return (
    <>
      <RobotMessage isOpen={isOpen} closeModal={closeModal} url={urlForAssistant} message={messageForAssintant} noShowLink={true}/>
			<RobotContainer>
        <h2>¿Cuál es tu presupuesto?</h2>
				<RobotIconContainer onClick={openModal} ml>
					<img src="/assets/bot.svg" alt="asistente online" />
				</RobotIconContainer>
			</RobotContainer>
      <BadgetRange setValuesToSend={setValuesToSend} setChangeValuesToSend={setChangeValuesToSend}/>
      <ButtonContainer>
        <NextButton onClick={handlePreviousStep}>Anterior</NextButton>
        <NextButton onClick={handleNextStep}>Siguiente</NextButton>
      </ButtonContainer>
    </>
  )
}
// setIsDisabled={setIsDisabled}
// disabled={isDisabled}