const manufacturer = {
  intel: [
    { min: '6741.99', max: '9401.99' },
    { min: '7437.99', max: '10297.99' },
    { min: '8807.99', max: '12119.99' },
    { min: '11062.99', max: '14970.00' },
    { min: '14128.00', max: '18316.00' },
    { min: '16033.00', max: '21616.00' },
    { min: '29229.99', max: '36942.99' },
    { min: '44363.99', max: '53023.99' },
    { min: '50975.99', max: '63522.99' },
  ],
  amd: [
    { min: '8136.99', max: '10796.99' },
    { min: '8629.99', max: '11489.99' },
    { min: '9588.99', max: '12900.99' },
    { min: '11545.99', max: '15453.00' },
    { min: '13602.00', max: '17790.00' },
    { min: '15958.00', max: '21541.00' },
    { min: '27641.99', max: '35354.99' },
    { min: '42468.99', max: '51128.99' },
    { min: '49749.99', max: '62026.99' },
  ]
}

export const getBudgetToSend = (budget, manufacturerState) => {
  const manufacturerSelected = manufacturer[manufacturerState]
  let budgetToSend;
  // console.log(manufacturerSelected, budget);

  if(manufacturerState === 'intel'){
    budgetToSend = getBudgetFromIntel(budget, manufacturerSelected)
    return budgetToSend
  }
  return budgetToSend = getBudgetFromAmd(budget, manufacturerSelected)
}

const getBudgetFromIntel = (budget, manufacturerSelected) => {
  if(budget <= 10000){
    return manufacturerSelected[0]
  }
  if(budget >= 10500 && budget < 12500){
    return manufacturerSelected[1]
  }
  if(budget >= 12500 && budget < 15000){
    return manufacturerSelected[2]
  }
  if(budget >= 15000 && budget < 18500){
    return manufacturerSelected[3]
  }
  if(budget >= 18500 && budget < 22000){
    return manufacturerSelected[4]
  }
  if(budget >= 22000 && budget < 37000){
    return manufacturerSelected[5]
  }
  if(budget >= 37000 && budget < 53500){
    return manufacturerSelected[6]
  }
  if(budget >= 53500 && budget < 64000){
    return manufacturerSelected[7]
  }
  if(budget >= 64000){
    return manufacturerSelected[8]
  }
}

const getBudgetFromAmd = (budget, manufacturerSelected) => {
  if(budget < 11500){
    return manufacturerSelected[0]
  }
  if(budget >= 11500 && budget < 13000){
    return manufacturerSelected[1]
  }
  if(budget >= 13000 && budget < 15500){
    return manufacturerSelected[2]
  }
  if(budget >= 15500 && budget < 18000){
    return manufacturerSelected[3]
  }
  if(budget >= 18000 && budget < 22000){
    return manufacturerSelected[4]
  }
  if(budget >= 22000 && budget < 35500){
    return manufacturerSelected[5]
  }
  if(budget >= 35500 && budget < 51500){
    return manufacturerSelected[6]
  }
  if(budget >= 51500 && budget < 62500){
    return manufacturerSelected[7]
  }
  if(budget >= 62500){
    return manufacturerSelected[8]
  }
}