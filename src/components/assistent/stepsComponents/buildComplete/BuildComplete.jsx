import { useRouter } from 'next/router';
import { useContext, useEffect, useState } from 'react'
import AppContext from '../../../../context/AppContext';
import { AssitantInstructions } from '../../../assistantInstructions/AssitantInstructions';
import { FifthStep } from './fifthStep/FifthStep';
import { FirstStep } from './firstStep/FirstStep'
import { FourthStep } from './fourthStep/FourthStep';
import { SecondStep } from './secondStep/SecondStep';
import { SixthStep } from './sixthStep/SixthStep';
import { ThirdStep } from './thirdStep/ThirdStep';

export const BuildComplete = () => {
  const { user, initialStepForBuildComplete } = useContext(AppContext)
  const [step, setStep] = useState(initialStepForBuildComplete);
  const router = useRouter()

  useEffect(() => {
    if(!user){
      router.replace('/login')
    }
  }, [user])

  const changeStep = (step) => {
    setStep(step)
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })
  }

  return (
    <>
      {step === 0 && <AssitantInstructions changeStep={changeStep}/> }
      {step === 1 && <FirstStep changeStep={changeStep}/> }
      {step === 2 && <SecondStep changeStep={changeStep}/> }
      {step === 3 && <ThirdStep changeStep={changeStep}/> }
      {step === 4 && <FourthStep changeStep={changeStep}/> }
      {step === 5 && <FifthStep changeStep={changeStep}/> }
      {step === 6 && <SixthStep changeStep={changeStep}/> }
    </>
  )
}
