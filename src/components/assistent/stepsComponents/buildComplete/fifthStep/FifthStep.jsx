import { useContext, useState, useEffect } from 'react';
import AppContext from '../../../../../context/AppContext';
import { Spinner } from '../../../../spinner/Spinner';
import { OptionCheckboxContainer, OptionIconContainer, OptionsContainer, NextButton, OptionLabel, ButtonContainer, } from "../../../assistentStyles";
import { useModal } from '../../../../../hooks/useModal';
import { robotMessages } from '../../../../../utils/robotMessages/robotMessages';
import { RobotMessage } from '../../../../robotMessage/RobotMessage';
import { RobotContainer, RobotIconContainer } from '../../../../robotMessage/robotMessageStyles';
import Swal from 'sweetalert2';

export const FifthStep = ({ changeStep }) => {
  const [listOfAccessories, setListOfAccessories] = useState([]);
  const { accessories, isFetching, setAccessories, getProducts, error, message } = useContext(AppContext);
	const [ isOpen, openModal, closeModal ] = useModal(false)
	const urlForAssistant = "https://www.google.com"
	const messageForAssintant = robotMessages.accessories.message()

  useEffect(() => {
    setListOfAccessories(accessories)
  }, [])

  const handleOnChange = (e) => {
		const { value } = e.target;
		if (listOfAccessories.includes(value)) {
			const newListOfAccessories = listOfAccessories.filter(
				(accessory) => accessory !== value
			);
			setListOfAccessories(newListOfAccessories);
			setAccessories(newListOfAccessories)
		} else {
			setListOfAccessories([...listOfAccessories, value]);
			setAccessories([...listOfAccessories, value]);
		}
	};

  const handleNextStep = () => {
    setAccessories(listOfAccessories)
		getProducts(changeStep, listOfAccessories)
  }

  const handlePreviousStep = () => {
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
    changeStep(4)
  }

  return (
		<>
			<RobotMessage isOpen={isOpen} closeModal={closeModal} url={urlForAssistant} message={messageForAssintant} noShowLink={true}/>
			<RobotContainer>
				<h2>Selecciona tus accesorios</h2>
				<RobotIconContainer onClick={openModal} ml>
					<img src="/assets/bot.svg" alt="asistente online" />
				</RobotIconContainer>
			</RobotContainer>
			<OptionsContainer>
				<OptionCheckboxContainer>
					<h3>Monitor</h3>
					<OptionIconContainer>
						<img src="/assets/screen.svg" alt="screen option" />
					</OptionIconContainer>
					<OptionLabel noBorder center>
						<input
							type="checkbox"
							name="screen"
							value="screen"
							onChange={handleOnChange}
              checked={listOfAccessories.some(item => item === 'screen')}
						/>{" "}
					</OptionLabel>
				</OptionCheckboxContainer>
				<OptionCheckboxContainer noBorder>
					<h3>Mouse</h3>
					<OptionIconContainer>
						<img
							src="/assets/mouse.svg"
							alt="Mouse option"
						/>
					</OptionIconContainer>
					<OptionLabel noBorder center>
						<input
							type="checkbox"
							name="mouse"
							value="mouse"
							onChange={handleOnChange}
              checked={listOfAccessories.some(item => item === 'mouse')}
						/>{" "}
					</OptionLabel>
				</OptionCheckboxContainer>
				<OptionCheckboxContainer noBorder center>
					<h3>Headset</h3>
					<OptionIconContainer>
						<img src="/assets/headset.svg" alt="headset option" />
					</OptionIconContainer>
					<OptionLabel noBorder center>
						<input
							type="checkbox"
							name="headset"
							value="headset"
							onChange={handleOnChange}
              checked={listOfAccessories.some(item => item === 'headset')}
						/>{" "}
					</OptionLabel>
				</OptionCheckboxContainer>
				<OptionCheckboxContainer noBorder>
					<h3>Teclado</h3>
					<OptionIconContainer>
						<img src="/assets/keyboard.svg" alt="keyboard option" />
					</OptionIconContainer>
					<OptionLabel noBorder center>
						<input
							type="checkbox"
							name="keyboard"
							value="keyboard"
							onChange={handleOnChange}
              checked={listOfAccessories.some(item => item === 'keyboard')}
						/>{" "}
					</OptionLabel>
				</OptionCheckboxContainer>
			</OptionsContainer>
			{error && <p>{message}</p> }
      <ButtonContainer>
        <NextButton onClick={handlePreviousStep}>Anterior</NextButton>
        <NextButton
          onClick={handleNextStep}
        >
          {isFetching ? <Spinner size='20' color='#FFFFFF'/> : 'Siguiente'}
        </NextButton>
      </ButtonContainer>
		</>
	);
}
