import { useRouter } from 'next/router';
import { useContext, useEffect } from 'react';
import AppContext from '../../../../../context/AppContext';
import { RobotMessage } from '../../../../robotMessage/RobotMessage';
import { RobotContainer, RobotIconContainer } from '../../../../robotMessage/robotMessageStyles';
import { ButtonContainer, NextButton, OptionIconContainer, OptionLabelContainer, OptionsContainer } from '../../../assistentStyles';
import { useModal } from '../../../../../hooks/useModal';
import { robotMessages } from '../../../../../utils/robotMessages/robotMessages';

export const FirstStep = ({ changeStep }) => {
	const { user, operatingSystem, setOperatingSystem, setCategory } = useContext(AppContext);
	const router = useRouter();
	const name = user ? user.displayName : ''
	const [ isOpen, openModal, closeModal ] = useModal(false)
	const urlForAssistant = "https://www.google.com"
	const messageForAssintant = robotMessages.operating_systems.message(name)

	useEffect(() => {
		if (!user) {
			router.replace("/login");
		}
	}, [user]);

	const handleOnClick = (option) => {
		setOperatingSystem(option);
		setTimeout(() => {
			window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
			changeStep(2);
		}, 200);
	};

	const handlePreviousStep = () => {
		router.push('/')
		window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
	};

	

	return (
		<>
			<RobotMessage isOpen={isOpen} closeModal={closeModal} url={urlForAssistant} message={messageForAssintant}/>
			<RobotContainer>
				<h2>Elije el sistema operativo</h2>
				<RobotIconContainer onClick={openModal} ml>
					<img src="/assets/bot.svg" alt="asistente online" />
				</RobotIconContainer>
			</RobotContainer>
			<OptionsContainer>
				<OptionLabelContainer onClick={() => handleOnClick("windows")}>
					<h3>Windows</h3>
					<OptionIconContainer>
						<img src="/assets/windows.svg" alt="windows operating system" />
					</OptionIconContainer>
					<input
						type="radio"
						name="quiz-option"
						defaultChecked={operatingSystem === 'windows'}
					/>
				</OptionLabelContainer>
				<OptionLabelContainer onClick={() => handleOnClick("ubuntu")}>
					<h3>Ubuntu</h3>
					<OptionIconContainer>
						<img src="/assets/ubuntu.svg" alt="ubuntu operating system" />
					</OptionIconContainer>
					<input
						type="radio"
						name="quiz-option"
            defaultChecked={operatingSystem === 'ubuntu'}
					/>
				</OptionLabelContainer>
			</OptionsContainer>
			<ButtonContainer>
				<NextButton onClick={handlePreviousStep}>Regresar a la página de inicio</NextButton>
				{/* <NextButton disabled={isDisabled} onClick={handleOnClick}>Siguiente</NextButton> */}
			</ButtonContainer>
		</>
	);
};
