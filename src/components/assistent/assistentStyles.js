import styled from "styled-components";
import device from "../../utils/css/breakpoints";

export const MainContainer = styled.div`
  width: 95%;
  margin: 0 auto;
  text-align: center;
  margin-bottom: 2rem;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-top: 8rem;

  & h2 {
    font-size: 1.7rem;
    margin-bottom: 2rem;
  }

  @media ${device.mobileL}{
    min-height: calc(100vh - 104px);
  }
`
export const OptionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  width: 95%;
  margin: 0 auto;
  margin-bottom: 2rem;

  @media ${device.mobileL}{
    flex-direction: row;
    justify-content: space-between;
  }
`
export const OptionLabelContainer = styled.label`
  /* width: 33%; */
  flex: 1;
  padding: 1rem;
  box-shadow: 2px 3px 9px -5px rgba(0 0 0 / 50%);
  margin-bottom: 1.5rem;

  &:hover {
    cursor: pointer;
  }

  @media ${device.mobileL}{
    margin-bottom: 0;
  }
`
export const OptionCheckboxContainer = styled.div`
  box-shadow: 2px 3px 9px -5px rgba(0 0 0 / 50%);
  flex: 1;
  padding: 1rem;
  margin-bottom: 1.5rem;

  @media ${device.mobileL}{
    margin-right: 1rem;
    margin-bottom: 0;
  }
`
export const OptionLabel = styled.label`
  display: flex;
  width: 100%;
  margin-bottom: .5rem;
  padding: .5rem;
  border-bottom: ${props => props.noBorder ? 'none' : '1px solid rgba(0, 0, 0, .2)'};
  justify-content: ${props => props.center ? 'center' : 'flex-start'};
  cursor: pointer;

  & input {
    margin-right: .75rem;
  }
`

export const OptionIconContainer = styled.div`
  width: 4rem;
  height: 4rem;
  margin: 0 auto;
  margin-bottom: 2rem;

  & img {
    width: 100%;
    height: 100%;
  }
`

export const ButtonContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  margin: 0 auto;
  width: 95%;

  @media ${device.mobileL}{
    flex-direction: row;
    width: 75%;
  }
`

export const NextButton = styled.button`
  border: none;
  cursor: pointer;
  padding: .7rem 3rem;
  border-radius: .5rem;
  font-size: 1rem;
  font-weight: 700;
  background-color: #303030;
  color: #FFFFFF;
  margin-bottom: 1rem;

  @media ${device.mobileL}{
    margin-right: 1rem;
  }
  

  &:disabled {
    background-color: gray;
    cursor: unset;
    box-shadow: 0px 2px 4px rgba(128, 128, 128, 0.4);
  }
`