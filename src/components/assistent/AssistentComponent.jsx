import { useContext, useEffect } from 'react'
import { MainContainer } from './assistentStyles'
import { BuildComplete } from './stepsComponents/buildComplete/BuildComplete'
import RobotContext from '../../context/robot/RobotContext';
import AppContext from '../../context/AppContext';


export const AssistentComponent = () => {
  const { showRobotLoginModal, setHideModal } = useContext(RobotContext)
  const { user } = useContext(AppContext)
  const category = 'complete_build'

  useEffect(() => {
    if(user && showRobotLoginModal){
      setHideModal()
    }
  }, [user, showRobotLoginModal])

  return (
    <MainContainer>
      {category === 'complete_build' && <BuildComplete/>}
    </MainContainer>
  )
}