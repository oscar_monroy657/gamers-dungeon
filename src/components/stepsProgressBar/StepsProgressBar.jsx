import { useEffect, useRef, useState } from 'react'
import { progressBarOptions } from './progressBarOptions';
import styles from './steps.module.css'

export const StepsProgressBar = ({ status, orderType }) => {
  const [orderStatus, setOrderStatus] = useState(progressBarOptions[orderType])
  const currentStatus = useRef(1)

  useEffect(() => {
    
    orderStatus.forEach((order, i) => {
      if(order.name === status){
        currentStatus.current = i + 1
      }
    })

    const newOrderStatus = orderStatus.map((oldStatus, i) => {
      if((i+1) <= currentStatus.current){
        return {
          ...oldStatus,
          isCurrent: true,
        }
      }
      return oldStatus
    })

    setOrderStatus(newOrderStatus)

  }, [])

	return (
		<div className={styles.container}>
			<ul className={styles.progressbar}>
        { orderStatus.map((status, i) => (
          <li className={`${status.isCurrent ? styles.active : ''}`} key={i}>
					  <span>{status.label}</span>
				  </li>
        ))}
				{/* <li className={styles.active}>
					<span>Revisión</span>
				</li>
				<li className={styles.active}>
					<span>Armando</span>
				</li>
				<li>
					<span>En ruta</span>
				</li>
				<li>
					<span>Entregado</span>
				</li> */}
			</ul>
		</div>
	);
};
