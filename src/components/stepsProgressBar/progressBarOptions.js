export const progressBarOptions = {
  'complete_building': [
    {
      name: 'checking',
      isCurrent: false,
      label: 'Revisión'
    },
    {
      name: 'building',
      isCurrent: false,
      label: 'Armando'
    },
    {
      name: 'on route',
      isCurrent: false,
      label: 'En ruta'
    },
    {
      name: 'delivered',
      isCurrent: false,
      label: 'Entregado'
    }
  ],
  'many_products': [
    {
      name: 'checking',
      isCurrent: false,
      label: 'Revisión'
    },
    {
      name: 'preparing',
      isCurrent: false,
      label: 'Preparando'
    },
    {
      name: 'on route',
      isCurrent: false,
      label: 'En ruta'
    },
    {
      name: 'delivered',
      isCurrent: false,
      label: 'Entregado'
    }
  ],
  'single_products': [
    {
      name: 'checking',
      isCurrent: false,
      label: 'Revisión'
    },
    {
      name: 'on route',
      isCurrent: false,
      label: 'Listo para recoger'
    },
    {
      name: 'delivered',
      isCurrent: false,
      label: 'Entregado'
    }
  ]
}