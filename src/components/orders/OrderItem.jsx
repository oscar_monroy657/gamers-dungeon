import moment from "moment"
import { useModal } from "../../hooks/useModal"
import { Modal } from "../modal/Modal"
import { StepsProgressBar } from "../stepsProgressBar/StepsProgressBar"
import { OrderItemContainer, OrderItemHeader, OrderItemInformation, OrderModalCost, OrderModalItem, OrdertTotalInformation, ViewDetailsButton } from "./orderItemStyles"

export const OrderItem = ({order}) => {
  const { id_factura, fecha_factura, total_venta, details, status, orderType } = order
  const [ isOpen, openModal, closeModal ] = useModal(false)

  return (
    <OrderItemContainer>
      <OrderItemHeader>
        <OrderItemInformation>
          <h3>Factura: {id_factura}</h3>
          <p>Fecha de solicitud: {moment(fecha_factura).format('DD/MM/YYYY')}</p>
        </OrderItemInformation>
        <OrdertTotalInformation>
          <p>Total: <strong>Q{Number(total_venta).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</strong></p>
        </OrdertTotalInformation>
      </OrderItemHeader>
      <OrderItemInformation>
        <h3>Estado:</h3>
        <StepsProgressBar status={status} orderType={orderType}/>
      </OrderItemInformation>
      <OrderItemInformation>
        <ViewDetailsButton type='button' onClick={openModal}>
          Ver detalles
        </ViewDetailsButton>
      </OrderItemInformation>
      <Modal isOpen={isOpen} closeModalHandle={closeModal} title={id_factura}>
        {details.map(detail => (
          <OrderModalItem key={detail.id_producto}>
            <p><strong>{detail.nombre_producto}</strong></p>
            <OrderModalCost>
              <p><span>Precio: </span>
                <strong>
                  Q {Number(detail.precio_venta).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                </strong>
              </p>
              <p><span>Cantidad:</span> <span>{detail.cantidad}</span></p>
              <p><span>Total: </span>
                <strong>
                  Q{Number(Number(detail.precio_venta) * Number(detail.cantidad)).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                </strong>
              </p>
            </OrderModalCost>
          </OrderModalItem>
        ))}
      </Modal>
    </OrderItemContainer>
  )
}
