import { MainContainer } from "../assistent/assistentStyles"
import { EmptyCartIconContainer } from "../emptyCart/emptyCartStyles"

export const NoOrders = () => {
  return (
    <MainContainer>
      <EmptyCartIconContainer>
      <img src='/assets/no_orders.svg' allt='Sin productos en el carrito.'/>
      <h2>No tienes órdenes.</h2>
    </EmptyCartIconContainer>
    </MainContainer>
  )
}
