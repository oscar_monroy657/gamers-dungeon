import styled from 'styled-components'
import device from '../../utils/css/breakpoints'

export const OrdersContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

export const OrderItemContainer = styled.div`
  box-shadow: 0 3px 11px -5px rgba(0,0,0, .3);
  border-radius: .5rem;
  display: flex;
  flex-direction: column;
  margin-bottom: 2rem;
  padding: 1rem;
  width: 100%;

  @media ${device.tablet}{
    width: 48%;
  }
`

export const OrderItemHeader = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-start;

  @media ${device.tablet}{
    flex-direction: row;
    justify-content: space-between;
  }
`
export const OrderItemInformation = styled.div`
  text-align: left;
`
export const OrdertTotalInformation = styled.div`
  text-align: left;

  @media ${device.tablet}{
    text-align: inherit;
  }
`

export const ViewDetailsButton = styled.button`
  border: 1px solid #1976D2;
  background-color: #FFFFFF;
  color: #1976D2;
  padding: .5rem;
  border-radius: .2rem;
  cursor: pointer;
  transition: background-color .3s ease-in-out;

  &:hover {
    background-color: rgba(25, 118, 210, .1);
  }
`
export const OrderModalItem = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-bottom: 1.5rem;

  & p {
    text-align: left;
  }
  
  & > p {
    margin-bottom: 0;
  }
`
export const OrderModalCost = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;

  & p {
    margin-right: 1rem;
    display: flex;
    flex-direction: column;
    text-align: center;

    @media ${device.tablet}{
      flex-direction: row;

      & span {
        margin-right: .5rem;
      }
    }
  }
`