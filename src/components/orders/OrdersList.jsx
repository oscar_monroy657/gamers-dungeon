import { MainContainer } from "../assistent/assistentStyles"
import { OrderItem } from "./OrderItem"
import { OrdersContainer } from "./orderItemStyles";

export const OrdersList = ({orders}) => {
  // console.log('Ordenes',orders);
  return (
    <MainContainer>
      <h2>Tus pedidos</h2>
      <OrdersContainer>
        {orders.map(order => (
          <OrderItem order={order} key={`${order.id_factura}${order.fecha_factura}`}/>
        ))}
      </OrdersContainer>
    </MainContainer>
  )
}
