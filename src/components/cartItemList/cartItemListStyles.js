import styled, { css } from 'styled-components';
import device from '../../utils/css/breakpoints';

export const CartContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;

  @media ${device.tablet}{
    flex-direction: row;
    justify-content: space-between;
  }
`

export const CartProductsTitle = styled.h2`
  display: flex;
  justify-content: space-between;

  & span {
    font-size: 1.2rem;
    color: #E53935;
    display: flex;
    align-items: center;
    cursor: pointer;

    & img {
      width: 1.4rem;
      height: 1.4rem;
    }
  }
`

export const CartProductsList = styled.div`
  width: 100%;

  @media ${device.tablet}{
    width: 63%;
  }
  @media ${device.laptop}{
    width: 65%;
  }
`
export const CartStripeElements = styled.div`
  width: 100%;

  @media ${device.tablet}{
    width: 35%;
  }
  @media ${device.laptop}{
    width: 33%;
  }
`

export const Total = styled.p`
  color: #a5a7ad;
  font-size: 1.2rem;

  & span {
    color: #000000;
    font-weight: 700;
  }
`