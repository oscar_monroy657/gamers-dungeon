import { useContext } from "react"
import CartContext from "../../context/cart/CartContext";
import { CartItem } from "../cartItem/CartItem";
import { CartProductsTitle, Total } from "./cartItemListStyles";

export const CartItemList = () => {
	const { cartItems, totalToPay, addItem, removeItem, cleanItemFromCart, clearCart } = useContext(CartContext);

	const removeProduct = (product) => {
		removeItem(product, cartItems);
	};

	const addProduct = (product) => {
		addItem(product, cartItems);
	};

	const deleteProduct = (product) => {
		cleanItemFromCart(product, cartItems);
	};

  return (
    <>
      <CartProductsTitle>
        Productos 
        <span onClick={clearCart}>
          Vaciar carrito.
          <img src="/assets/delete_1.svg" alt="Clear cart" />
        </span>
      </CartProductsTitle>
        <div>
          { cartItems.sort((a, b) => {
            // console.log('Ordenando', a, b);
                if ((a.nombre_producto ?? a.nombre) < (b.nombre_producto ?? b.nombre)) { return -1; } if ((a.nombre_producto ?? a.nombre) > (b.nombre_producto ?? b.nombre)) { return 1; } return 0;
            }).map((product, index) => (
              <CartItem
                product={product} 
                key={product.codigo_producto} 
                removeProduct={removeProduct}
                addProduct={addProduct}
                deleteProduct={deleteProduct}
                index={index}
              />
            ))
          }
          </div>
        <Total>Total a pagar: <span>Q{Number(totalToPay ?? 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</span></Total>
    </>
  )
}