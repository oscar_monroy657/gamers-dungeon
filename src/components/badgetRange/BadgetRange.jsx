import { useContext, useState, useEffect } from "react";
import { Range, getTrackBackground } from "react-range";
import AppContext from "../../context/AppContext";
import { BadgetRangeContainer } from "./badgetRangeStyles";

const STEP = 500;

export const BadgetRange = ({ setValuesToSend, setChangeValuesToSend }) => {
	const { budgetLimits, budgetToShow, setBudgetToShow } = useContext(AppContext);
	const MIN = budgetLimits.min;
	const MAX = budgetLimits.max;
	const [values, setValues] = useState([15000]);
	const rtl = false;

	useEffect(() => {
		if(budgetToShow < budgetLimits.min){
			setValues([15000])
			setBudgetToShow([15000])
			setValuesToSend([15000])
			setChangeValuesToSend(true)
			return
		}
	  setValues(budgetToShow)
	}, [])

	const handleChange = (values) => {
		setValuesToSend(values);
		setValues(values);
    setBudgetToShow(values)
	};

	return (
		<BadgetRangeContainer>
			<Range
				values={values}
				step={STEP}
				min={MIN}
				max={MAX}
				rtl={rtl}
				onChange={(values) => handleChange(values)}
				renderTrack={({ props, children }) => (
					<div
						onMouseDown={props.onMouseDown}
						onTouchStart={props.onTouchStart}
						style={{
							...props.style,
							height: "36px",
							display: "flex",
							width: "100%",
						}}
					>
						<div
							ref={props.ref}
							style={{
								height: "5px",
								width: "100%",
								borderRadius: "4px",
								background: getTrackBackground({
									values,
									colors: ["#548BF4", "#ccc"],
									min: MIN,
									max: MAX,
									rtl,
								}),
								alignSelf: "center",
							}}
						>
							{children}
						</div>
					</div>
				)}
				renderThumb={({ props, isDragged }) => (
					<div
						{...props}
						style={{
							...props.style,
							height: "42px",
							width: "42px",
							borderRadius: "4px",
							backgroundColor: "#FFF",
							display: "flex",
							justifyContent: "center",
							alignItems: "center",
							boxShadow: "0px 2px 6px #AAA",
						}}
					>
						<div
							style={{
								height: "16px",
								width: "5px",
								backgroundColor: isDragged ? "#548BF4" : "#CCC",
							}}
						/>
					</div>
				)}
			/>
			<output style={{ marginTop: "2rem", marginBottom: '2rem', }} id="output">
				Presupuesto máximo: Q{values[0].toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
			</output>
		</BadgetRangeContainer>
	);
};
