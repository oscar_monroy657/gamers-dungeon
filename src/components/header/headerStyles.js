import styled from 'styled-components';
import device from '../../utils/css/breakpoints';

// #303030 gradient.

export const HeaderContainer = styled.header`
  background-color: #202020;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: space-around;
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 1000;

  @media ${device.tablet}{
    justify-content: space-between;
    padding-left: 2rem;
    padding-right: 2rem;
  }

`
export const HeaderItems = styled.ul`
  display: none;
  flex-wrap: wrap;
  width: 100%;
  justify-content: space-around;
  padding: 0;
  align-items: center;
  color: #eee;
  list-style: none;
  margin: 0;

  @media ${device.tablet} {
    display: flex;
    flex-wrap: nowrap;
    width: 50%;
    justify-content: flex-end;
  }
`
export const HeaderMobileItems = styled.ul`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  justify-content: flex-end;
  padding: 0;
  align-items: center;
  color: #eee;
  list-style: none;
  margin: 0;
  margin-right: 1rem;

  @media ${device.tablet} {
    display: none;
  }

`

export const HeaderItem = styled.li`
  margin-bottom: 1rem;
  cursor: pointer;
  &:hover {
    color: #FFFFFF;
  }

  @media ${device.tablet}{
    margin-bottom: 0;
    margin-left: .5rem;
  }

`
export const LinkItem = styled.a`
  display: flex;
  width: 6rem;
  height: 3rem;
  justify-content: center;
  align-items: center;
  color: ${(props) => props.isActive ? 'red': 'inherit'};
`
export const LogoutButton = styled.button`
  display: flex;
  width: 7rem;
  height: 3rem;
  justify-content: center;
  align-items: center;
  color: inherit;
  background-color: transparent;
  border: none;
  font-family: 'Roboto', sans-serif;
  font-size: 1rem;
  cursor: pointer;
`
// Menu mobile
export const MenuMobileContainer = styled.div`
  width: 100%;
  background-color: #120f13;
  display: flex;
  height: 100vh;
  min-height: 100vh;
  overflow: hidden;
  position: fixed;
  z-index: 11;
  bottom: 0;
  transition: left .7s ease-in-out;
  left: ${(props) => (props.isOpen ? '0rem' : '-100%')};

  @media ${device.tablet} {
    display: none;
  }
`
export const MenuMobileContent = styled.ul`
  background-color: #120f13;
  color: #e0e0e0;
  width: 100%;
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  position: relative;
  list-style: none;
  align-items: center;
  justify-content: center;
  padding: 0;
`
export const CloseMobileContainer = styled.div`
  width: 12%;
  display: flex;
  justify-content: center;
  max-height: 100vh;
  padding-top: 0.8rem;
  position: relative;
`
export const CloseMobileButton = styled.div`
box-shadow: 0px 4px 4px 0px #00000040;
  background-color: #120f13;
  height: 2rem;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 0.8rem;
  padding: 0.2rem;
  width: 2rem;

  & img {
    width: 1.5rem;
    height: 1.5rem;
  }
`
export const OpenMenuButton = styled.button`
  border: none;
  background: none;
  display: flex;
  align-items: center;
  width: 45px;
  height: 48px;

  & img {
    width: 100%;
    height: 100%;
  }
`