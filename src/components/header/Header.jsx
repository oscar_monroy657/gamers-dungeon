import { useRouter } from "next/router";
import { useState } from "react";
import { useContext } from "react";
import AppContext from "../../context/AppContext";
import { CartIcon } from "../cartIcon/CartIcon";
import { UserHeaderOption } from "../userHeaderOption/UserHeaderOption";
import { ActiveLink, ImageLink } from "./ActiveLink";
import {
	HeaderContainer,
	HeaderItem,
	HeaderItems,
	HeaderMobileItems,
	LogoutButton,
  OpenMenuButton,
} from "./headerStyles";
import { MenuMobile } from "./MenuMobile";
import RobotContext from '../../context/robot/RobotContext';

export const Header = () => {
	const router = useRouter();
	const { user, signOutApp } = useContext(AppContext);
	const { setPageToShow } = useContext(RobotContext)
	const [isMenuOpen, setIsMenuOpen] = useState(false);

	const handleClick = () => {
		setPageToShow('/crearEquipo')
		signOutApp();
	};

	const setMenuStatus = () => {
		setIsMenuOpen(!isMenuOpen);
	};

	return (
		<HeaderContainer>
			<ImageLink />
			<HeaderItems>
				<HeaderItem>
					<ActiveLink href="/" currentPath={router.asPath} linkLabel="Inicio" />
				</HeaderItem>
				<HeaderItem>
					<ActiveLink href="/productos" currentPath={router.asPath} linkLabel="Productos" />
				</HeaderItem>
				<HeaderItem>
					<ActiveLink href="/desks" currentPath={router.asPath} linkLabel="Escritorios" />
				</HeaderItem>
				<HeaderItem>
					<ActiveLink
						href="/servicios"
						currentPath={router.asPath}
						linkLabel="Servicios"
					/>
				</HeaderItem>
				<HeaderItem>
					<ActiveLink
						href="/contacto"
						currentPath={router.asPath}
						linkLabel="Contacto"
					/>
				</HeaderItem>
				<HeaderItem>
					<CartIcon />
				</HeaderItem>
				<HeaderItem>
					<UserHeaderOption user={user} logoutHandle={handleClick}/>
				</HeaderItem>
			</HeaderItems>
      <HeaderMobileItems>
        <HeaderItem>
					<CartIcon />
				</HeaderItem>
        <HeaderItem>
          <OpenMenuButton type='button' onClick={setMenuStatus}>
            <img src="/assets/menu.svg" alt="Open menu" />
          </OpenMenuButton>
        </HeaderItem>
      </HeaderMobileItems>
			<MenuMobile
				isOpen={isMenuOpen}
				user={user}
				signOut={handleClick}
				router={router}
        closeMenu={setMenuStatus}
			/>
		</HeaderContainer>
	);
};
