import { ActiveLink } from "./ActiveLink";
import {
	CloseMobileButton,
	HeaderItem,
	LogoutButton,
	MenuMobileContainer,
	MenuMobileContent,
} from "./headerStyles";

export const MenuMobile = ({ isOpen, user, signOut, router, closeMenu }) => {
	return (
		<MenuMobileContainer isOpen={isOpen}>
			<MenuMobileContent>
				<HeaderItem>
					<ActiveLink href="/" currentPath={router.asPath} linkLabel="Inicio" />
				</HeaderItem>
				<HeaderItem>
					<ActiveLink
						href="/productos"
						currentPath={router.asPath}
						linkLabel="Productos"
					/>
				</HeaderItem>
				<HeaderItem>
					<ActiveLink
						href="/desks"
						currentPath={router.asPath}
						linkLabel="Escritorios"
					/>
				</HeaderItem>
				<HeaderItem>
					<ActiveLink
						href="/servicios"
						currentPath={router.asPath}
						linkLabel="Servicios"
					/>
				</HeaderItem>
				<HeaderItem>
					<ActiveLink
						href="/contacto"
						currentPath={router.asPath}
						linkLabel="Contacto"
					/>
				</HeaderItem>
				{user && (
					<HeaderItem>
						<ActiveLink
							href="/crearEquipo"
							currentPath={router.asPath}
							linkLabel="Asistente"
						/>
					</HeaderItem>
				)}
				{user && (
					<HeaderItem>
						<ActiveLink
							href="/pedidos"
							currentPath={router.asPath}
							linkLabel="Mis pedidos"
						/>
					</HeaderItem>
				)}
				{user ? (
					<>
						<HeaderItem>
							<LogoutButton onClick={signOut}>Cerrar sesión</LogoutButton>
						</HeaderItem>
					</>
				) : (
					<HeaderItem>
						<ActiveLink
							href="/login"
							currentPath={router.asPath}
							linkLabel="Ingresar"
						/>
					</HeaderItem>
				)}
				<CloseMobileButton>
					<img src="/assets/close.svg" alt="Close menu" onClick={closeMenu} />
				</CloseMobileButton>
			</MenuMobileContent>
			{/* <CloseMobileContainer>
        <CloseMobileButton>
          <img src="/assets/close.svg" alt="Close menu" onClick={closeMenu}/>
        </CloseMobileButton>
      </CloseMobileContainer> */}
		</MenuMobileContainer>
	);
};
