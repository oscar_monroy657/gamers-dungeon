import Link from 'next/link';
import Image from 'next/image';
import { LinkItem } from './headerStyles';
import logo from '../../../public/assets/logo2.png';

export const ActiveLink = ({href, currentPath, linkLabel}) => {
  const isActive = href === currentPath ? true : false;

  return (
    <Link href={href} passHref>
      <LinkItem isActive={isActive}>{linkLabel}</LinkItem>
    </Link>
  )
}

export const ImageLink = () => (
  <Link href='/'>
    <a>
      <Image 
        src={logo} 
        alt='Page logo' 
        layout='fixed'
        width={200}
        height={100}
      />
    </a>
  </Link>
)
