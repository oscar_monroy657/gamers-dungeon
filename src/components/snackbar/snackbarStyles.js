import styled from "styled-components";
import device from '../../utils/css/breakpoints';

export const SnackBarContainer = styled.div`
  display:flex;
  justify-content: space-between;
  width: 100vw;
  background:#222;
  border:#f2f2f2;
  padding:10px 40px 10px 20px;
  color:#fff;
  position:fixed;
  bottom: ${props => props.showSnackBar ? '-1%' : '-100%'};
  transition: bottom 0.75s ease-in-out;
  z-index:1001;
  border-radius: .25rem;
  
  @media ${device.tablet}{
    width: 45vw;
    left: 50%;
    transform: translateX(-50%);
    bottom: ${props => props.showSnackBar ? '1%' : '-100%'};
  }
`
export const SnackBarAction = styled.button`
  border: none;
  background: none;
  margin: 0;
  padding: .75rem;
  font-size: 14px;
  text-transform: uppercase;
  color: wheat;
  font-weight: 700;
  cursor: pointer;
  letter-spacing: 1px;
`