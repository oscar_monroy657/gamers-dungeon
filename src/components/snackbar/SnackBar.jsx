import { useRouter } from "next/router";
import { useContext } from "react";
import SnackBarContext from "../../context/snackbar/SnackBarContext";
import { SnackBarAction, SnackBarContainer } from "./snackbarStyles"
import RobotContext from '../../context/robot/RobotContext';

export const SnackBar = () => {
  const { showSnackBar } = useContext(SnackBarContext)
  const { setPageToShow } = useContext(RobotContext)
  const router = useRouter()

  const handleClick = () => {
    setPageToShow('/checkout')
    router.push('/checkout')
  }

  return (
    <SnackBarContainer showSnackBar={showSnackBar}>
      <p>¡Agregado al carrito!</p>
      <SnackBarAction onClick={handleClick}>Ver</SnackBarAction>
    </SnackBarContainer>
  )
}
