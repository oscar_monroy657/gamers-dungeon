import styled from "styled-components";

export const AccountOptionsContainer = styled.ul`
	list-style: none;
	background-color: #fff;
	padding: 1rem;
	color: #000;
	position: absolute;
	top: 100%;
	width: 200px;
	right: -100%;
	display: ${props => props.open ? 'flex' : 'none'};
	flex-direction: column;
	align-items: center;
	box-shadow: 0 3px 11px -5px rgba(0 0 0 / 30%);
  transition: display .4s ease-in-out;
  border-radius: .5rem;

  & li{
    width: 100%;
    margin-bottom: .5rem;
    font-weight: 700;
    display: flex;
    justify-content: center;

    & button {
      font-weight: 700;
    }
  }
`;

export const AccountOptionsItem = styled.a`
  display: block;
  width: 100%;
  padding: .5rem;
`
