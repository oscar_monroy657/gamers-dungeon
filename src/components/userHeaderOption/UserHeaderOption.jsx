import { useState } from "react";
import Link from "next/link";
import { CartIconContainer, Icon } from "../cartIcon/cartIconStyles";
import { LogoutButton } from "../header/headerStyles";
import { AccountOptionsContainer, AccountOptionsItem } from "./userHeaderOptionStyles";

export const UserHeaderOption = ({ user, logoutHandle }) => {
	const [isOpen, setIsOpen] = useState(false);
	return (
		<CartIconContainer onClick={() => setIsOpen(!isOpen)}>
			<Icon src="/assets/account_icon.svg" />
			<AccountOptionsContainer open={isOpen}>
				{user && (
					<li>
						<Link href="/crearEquipo" passHref>
            <AccountOptionsItem>Asistente</AccountOptionsItem>
						</Link>
					</li>
				)}
				{user && (
					<li>
						<Link href="/pedidos" passHref>
            <AccountOptionsItem>Mis pedidos</AccountOptionsItem>
						</Link>
					</li>
				)}
				<li>
					{user ? (
						<LogoutButton onClick={logoutHandle}>Cerrar sesión</LogoutButton>
					) : (
						<Link href="/login" passHref>
							<AccountOptionsItem>Iniciar sesión</AccountOptionsItem>
						</Link>
					)}
				</li>
			</AccountOptionsContainer>
		</CartIconContainer>
	);
};
