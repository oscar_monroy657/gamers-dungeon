import Image from "next/image"
import { useRouter } from "next/router";
import { useContext } from "react";
import servicesBackOne from '../../../public/assets/backServices18.png';
import { ContentContainer, ContentDescription, ContentTitle, CornerImage, MainContainer, PrimaryButton } from "../globalStyles";
import RobotContext from '../../context/robot/RobotContext';

export const ServicesContent = () => {
  const router = useRouter();
  const { setShowModal } = useContext(RobotContext)

  const handleClick = () => {
    setShowModal();
    router.push('/crearEquipo');
  }

  return (
    <MainContainer>
      <ContentContainer>
        <ContentTitle>Nuestros servicios</ContentTitle>
        <ContentDescription>Nuestra misión primordial es ayudarte en todo el proceso que seleccione para que tengas las mejores opciones para conseguir la computadora que necesitas, dando asesoría paso a paso, con el fin de bríndarte opciones que se acomoden a tus necesidades y presupuesto. Porque es lo que realmente te mereces.</ContentDescription>

        <ContentTitle>Sólo escritorio</ContentTitle>
        <ContentDescription>Ofrecemos un escritorio con posibilidades de configuración de hardware mas abierta a los rendimientos que necesite, ofrecemos 3 tamaños pensados para optimizar el rendimiento según las necesidades que requiera, dejándolo como la opción #1 100% guatemalteca.</ContentDescription>

        <ContentTitle>Armado completo</ContentTitle>
        <ContentDescription>¿Quieres armar una computadora, pero no sabes por dónde empezar? No te preocupes, nosotros te asesoraremos paso a paso, donde trataremos de ajustarnos a tus necesidades y presupuesto, dándote las piezas compatibles en cada escritorio para que sea mas sencillo encontrar la mejor opción según tus deseos</ContentDescription>
        
        <ContentTitle>Escritorio y algunas piezas</ContentTitle>
        <ContentDescription>Si ya cuentas con una computadora, pero quieres hacerle un upgrade, nosotros te ayudaremos ofreciéndote alguna de las opciones de escritorio y, según las piezas de hardware que ya posees, te ayudaremos dándote opciones compatibles para mejorar tu computadora, todo en tu nuevo case que te servirá para tu siguiente mejora.</ContentDescription>
        <div style={{ paddingTop: '1rem' }}>
          <PrimaryButton services onClick={handleClick}>Solicitar asesoría</PrimaryButton>
        </div>
        
      </ContentContainer>
      <CornerImage>
        <Image
          src={servicesBackOne}
          alt='Background image'
          layout='fill'
          objectFit='fill'
        />
      </CornerImage>
    </MainContainer>
  )
}
