import styled from 'styled-components';

export const InputLabel = styled.label`
	display: flex;
	flex-direction: column;
	font-weight: 700;
	font-size: 1.2rem;
	margin-bottom: 2rem;
	width: 100%;

	& span {
		margin-bottom: 0.75rem;
		color: #333;
	}
`;

export const InputText = styled.input`
	padding: 0.5rem 0;
	border: none;
	border-bottom: 2px solid #cdcfd3;
	color: #a5a7ad;
	font-family: 'Roboto', sans-serif;
	font-size: 1.1rem;
	font-weight: 700;
	line-height: 1.5rem;
	transition: border 1s ease-in-out;

	&:focus {
		outline: none;
		border-color: #333;
	}
`

export const TextArea = styled.textarea`
	padding: 0.5rem 0;
	border: none;
	border-bottom: 2px solid #cdcfd3;
	color: #a5a7ad;
	font-family: 'Roboto', sans-serif;
	font-size: 1.1rem;
	font-weight: 700;
	line-height: 1.5rem;
	transition: border 1s ease-in-out;

	&:focus {
		outline: none;
		border-color: #333;
	}
  &::-webkit-scrollbar {
		width: 10px;
	}
	&::-webkit-scrollbar-thumb {
		background: #b3b3b3;
		border-radius: 4px;
	}
	&::-webkit-scrollbar-track {
		background: #e1e1e1;
		border-radius: 4px;
	}
`;