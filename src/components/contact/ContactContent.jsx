import { useEffect, useState } from "react";
import { useContext } from "react";
import Image from "next/image"
import { TextArea, InputLabel, InputText } from "./contactContentStyles";
import servicesBackOne from '../../../public/assets/backServices18.png';
import { ContentContainer, ContentDescription, ContentTitle, CornerImage, MainContainer, PrimaryButton } from "../globalStyles";
import AppContext from "../../context/AppContext";
import axios from "axios";
import { Spinner } from "../spinner/Spinner";

export const ContactContent = () => {
  const { user } = useContext(AppContext)
  const [email, setEmail] = useState('')
  const [message, setMessage] = useState('')
  const [messageAlert, setMessageAlert] = useState('')
  const [isFetching, setIsFetching] = useState(false)

  useEffect(() => {
    if(user){
      setEmail(user.email)
    }else {
      setEmail('')
    }
  }, [user])

  const onHandleMessageChange = e => {
    const { value } = e.target
    setMessage(value)
  }

  const onHandleEmailChange = e => {
    const { value } = e.target
    setEmail(value)
  }

  const handleOnClick = async () => {
    // console.log(email, message);
    if(email.trim() === '' || message.trim() === ''){
      return
    }

    setIsFetching(true)
    const messageData = {
      email_to: email,
      content: message
    }

    const messageDataStr = JSON.stringify(messageData)
    // const messageJson = JSON.parse(messageDataStr)
    const messageJson = {
      "email_to":email,
      "content":message
    }
    // console.log(messageJson);

    try {
      const emailRequest = await axios.post('https://gamersdungeon-api.herokuapp.com/products-api/v1/mail', messageJson);
      setIsFetching(false)
      setMessageAlert('Correo enviado con éxito!')
      setTimeout(() => {
        setMessageAlert('')
      }, 3000);

    } catch (error) {
      console.log(error);
      setMessageAlert('Ha ocurido un error.')
      setIsFetching(false)
      setTimeout(() => {
        setMessageAlert('')
      }, 3000);
    }
  }

  return (
    <MainContainer>
      <ContentContainer>
        <ContentTitle>Escríbenos</ContentTitle>
        <ContentDescription>Nos interesa saber más de lo que necesites, estamos trabajando para que encuentres las mejores opciones posibles según tus necesidades, coméntanos las dudas que tengas y lo más pronto posible te ayudaremos con una solución.</ContentDescription>
        <form>
        {!user && (
            <InputLabel>
              <span>Correo electrónico</span>
              <InputText type="email" name="email" onChange={onHandleEmailChange} value={email} required/>
            </InputLabel>
          )}
          <InputLabel>
            <span>Mensaje</span>
            <TextArea name="message" rows={3} onChange={onHandleMessageChange} required value={message}></TextArea>
          </InputLabel>
          <div>
            <p>{messageAlert}</p>
          </div>
        </form>
        <PrimaryButton services onClick={handleOnClick}>
        {isFetching ? <Spinner size='20' color='#FFFFFF'/> : 'Enviar'}
        </PrimaryButton>
      </ContentContainer>
      <CornerImage>
        <Image
          src={servicesBackOne}
          alt='Background image'
          layout='fill'
          objectFit='fill'
        />
      </CornerImage>
    </MainContainer>
  )
}

{/* <TextArea name="description" rows={3} onChange={handleOnChange} value={description} ></TextArea> */}
