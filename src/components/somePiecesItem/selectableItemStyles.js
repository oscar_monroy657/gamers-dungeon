import styled from 'styled-components'
import device from '../../utils/css/breakpoints'

export const ItemLabel = styled.label`
  box-shadow: 2px 3px 9px -5px rgba(0 0 0 / 50%);
  width: 100%;
  margin-bottom: 2rem;
  padding-bottom: 1rem;

  @media ${device.mobileL}{
    width: 30%;
    margin-right: 1rem;
  }

  &:hover {
    cursor: pointer;
  }
`

export const ItemImage = styled.div`
  width: 100%;
  height: 175px;

  & img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
  }
`
export const ItemTitle = styled.h2`
  font-size: 1rem !important;
  color: #666;
  text-align: center;
  padding: 0 1rem;
`
