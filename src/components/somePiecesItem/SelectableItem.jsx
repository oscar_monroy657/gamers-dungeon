import { ItemImage, ItemLabel, ItemTitle } from './selectableItemStyles'

export const SelectableItem = ({ item, itemSelected, setSelectedItem }) => {
	const handleOnChange = () => {
		setSelectedItem({
			name: item.nombre_producto,
			product: {
				id_producto: item.id_producto,
				cantidad: 1,
			},
		});
	};

	return (
		<ItemLabel>
			<ItemImage>
				<img src={item.img_url} alt={item.nombre_producto} />
			</ItemImage>
			<ItemTitle>{item.nombre_producto}</ItemTitle>
			<p>
				Precio:{' '}
				<strong>
					Q
					{Number(item.precio_producto ?? 0)
						.toFixed(2)
						.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
				</strong>
			</p>
			<input 
        type='radio' 
        name='quiz-option' 
        onChange={handleOnChange} 
        defaultChecked={item.nombre_producto === itemSelected.name}
      />
		</ItemLabel>
	);
};
