import Head from "next/head";
import { createGlobalStyle } from "styled-components";
import { Footer } from "../footer/Footer";
import { Header } from "../header/Header";
import { SnackBar } from "../snackbar/SnackBar";

const GlobalStyle = createGlobalStyle`
  html {
    scroll-behavior: smooth;
  }
  body {
    font-family: 'Roboto', sans-serif;
    margin: 0;
    padding: 0;
  }
  a {
    color: inherit;
    text-decoration: none;
    cursor: pointer;
    text-align: center;
  }

  * {
    box-sizing: border-box;
  }
  #information:target{
    padding-top: 100px;
  }

  @media (min-width: 768px){
    #information:target{
    padding-top: 50px;
  } 
}
`;

export const Layout = ({ children }) => {
	return (
		<>
			<Head>
				<title>Gamers Dungeon</title>
				<meta name="description" content="El espacio donde puedes encontrar una computadora a tu medida." />
				<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
				<link
					href="https://fonts.googleapis.com/css2?family=Hammersmith+One&family=Roboto&display=swap"
					rel="stylesheet"
				/>
				<link rel="icon" href="/favicon.ico" />
        <meta name="facebook-domain-verification" content="vbjj4rfrp8t59wl80sxec8kaqsmyfr" />
        <meta name="description" content="El espacio donde puedes encontrar una computadora a tu medida."/>
        <meta
          property="og:url"
          content="https://gamersdungeon.store/"
        />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Gamers Dungeon"
        />
        <meta
          property="og:description"
          content="El espacio donde puedes encontrar una computadora a tu medida."
        />
        <meta
          property="og:image"
          content="https://firebasestorage.googleapis.com/v0/b/gamers-dungeon.appspot.com/o/gamersHome.png?alt=media&token=d13c6708-07e4-4531-9f7c-886cea226d10"
        />
			</Head>
			<GlobalStyle />
			<Header />
			{children}
      <SnackBar/>
			<Footer />
		</>
	);
};
