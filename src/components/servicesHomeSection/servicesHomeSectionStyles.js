import styled from "styled-components";
import device from "../../utils/css/breakpoints";

export const ServicesHomeItem = styled.div`
	flex: ${props => props.width ? 'auto' : '1'};
	padding: 1rem;
	box-shadow: 0 3px 11px -5px rgba(0 0 0 / 30%);
	margin-bottom: 1.5rem;
  margin-right: 0;

	cursor:  ${props => props.pointer ? 'pointer' : 'default'};

	@media ${device.mobileL} {
		margin-bottom: 0;
    margin-right: ${props => props.mr ? '1.5rem' : '0'};
	}
`;