import { ButtonContainer, NextButton, OptionIconContainer, OptionsContainer } from '../assistent/assistentStyles'
import { ItemTitle } from '../listOfProducts/productItemStyles'
import { ServicesHomeItem } from "./servicesHomeSectionStyles"
import { useRouter } from "next/router"
import { useContext } from 'react'
import RobotContext from '../../context/robot/RobotContext';
import * as gtag from '../../../utils/gtag'

export const ServicesHomeSection = () => {
  const router = useRouter()
  const { setShowModal, setPageToShow } = useContext(RobotContext)

  const sendOptionsSelectedOften = (optionText) => {
    gtag.event({
      action: 'option_often_selected',
      category: 'options_often',
      label: optionText,
      value: optionText
    })
  }

  return (
    <OptionsContainer id='information'>
      <ServicesHomeItem mr>
      <OptionIconContainer>
        <img src='/assets/desk.svg' alt='Escritorios' />
      </OptionIconContainer>
      <ItemTitle>Escritorios</ItemTitle>
      <p style={{ marginBottom: '2rem' }}>
      Ofrecemos escritorios con posibilidades de configuración más abierta a los rendimientos que necesite, dejándolo como <strong>la opción #1 100% guatemalteca.</strong>
			</p>
      <ButtonContainer>
          <NextButton onClick={() => {
            sendOptionsSelectedOften('Desk')
            router.push('/desks')
          }}
          >
              Ver escritorios
          </NextButton>
        </ButtonContainer>
    </ServicesHomeItem>

      <ServicesHomeItem mr>
      <OptionIconContainer>
        <img src='/assets/bot.svg' alt='asistente online' />
      </OptionIconContainer>
      <ItemTitle>Asistente</ItemTitle>
      <p style={{ marginBottom: '2rem' }}>
      <strong>¡Hola Soy Desky!</strong> un asistente que, a través de una serie de pasos, podrá recomendarte el equipo que mejor se adapta a tus necesidades y desafíos diarios.
			</p>
      <ButtonContainer>
          <NextButton onClick={() => {
            setShowModal()
            sendOptionsSelectedOften('Assistant')
            setPageToShow('/crearEquipo')
            router.push('/crearEquipo')
          }}>
            ¡Empezar!
          </NextButton>
        </ButtonContainer>
    </ServicesHomeItem>

      <ServicesHomeItem>
      <OptionIconContainer>
        <img src='/assets/motherboard.svg' alt='Piezas' />
      </OptionIconContainer>
      <ItemTitle>Piezas individuales</ItemTitle>
      <p style={{ marginBottom: '2rem' }}>
      Si eres un cliente con experiencia y estás buscando piezas en específico para armar tu pc, ¡también puedes adquirir piezas individuales!
			</p>
      <ButtonContainer>
          <NextButton onClick={() => {
            sendOptionsSelectedOften('Products')
            router.push('/productos')
          }}>
            Ver productos
          </NextButton>
        </ButtonContainer>
    </ServicesHomeItem>
    </OptionsContainer>
  )
}
