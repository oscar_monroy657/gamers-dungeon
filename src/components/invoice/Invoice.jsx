import { useRouter } from "next/router";
import { useContext } from "react";
import AppContext from "../../context/AppContext";
import CartContext from "../../context/cart/CartContext";
import { NextButton } from "../assistent/assistentStyles";
import { InvoiceContainer, InvoiceResume } from "./invoiceStyles";

export const Invoice = ({ invoice }) => {
  const { setCategory, setChallenges, setAccessories, setManufacturer, setOperatingSystem } = useContext(AppContext)
	const { setInvoiceData } = useContext(CartContext)
  const router = useRouter()

	const handleOnClick = () => {
		setCategory('initial')
    setChallenges([])
    setAccessories([])
    setManufacturer('')
    setOperatingSystem('')
		setInvoiceData(null)
    router.replace('/')
    window.scrollTo({ top: 0, left: 0, behavior: 'smooth'});
  }

	return (
		<InvoiceContainer>
			<h1>Datos de la factura</h1>
			<InvoiceResume>
				<p>
					Factura No. <strong>{invoice.numero_factura}</strong>
				</p>
			</InvoiceResume>
			{invoice.detalle.map((product) => (
				<InvoiceResume key={product.id_producto} border>
					<h2>{product.nombre_producto}</h2>
					<p>
						Precio:{" "}
						<strong>
							Q
							{Number(product.precio_venta ?? 0)
								.toFixed(2)
								.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
						</strong>
					</p>
				</InvoiceResume>
			))}
			<InvoiceResume marginBottom>
				<p>
					<strong>Total:</strong>
				</p>
				<p>
					Total:{" "}
					<strong>
						Q
						{Number(invoice.total_venta ?? 0)
							.toFixed(2)
							.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
					</strong>
				</p>
			</InvoiceResume>
			<NextButton onClick={handleOnClick}>Aceptar</NextButton>
		</InvoiceContainer>
	);
};
