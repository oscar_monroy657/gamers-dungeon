import { useRouter } from "next/router"
import { useContext, useEffect } from "react"
import CartContext from "../../context/cart/CartContext"
// import AppContext from "../../context/AppContext"
import { MainContainer } from "../assistent/assistentStyles"
// import { MainContainer, NextButton } from "../assistent/assistentStyles"
import { Invoice } from "./Invoice"

export const InvoiceContainer = () => {
  const { invoice } = useContext(CartContext)
  // const { setCategory } = useContext(AppContext)
  const router = useRouter()

  useEffect(() => {
    // console.log(invoice);
    if(!invoice){
      router.replace('/')
    }
  }, [])

  // const handleOnClick = () => {
  //   setCategory('initial')
  //   router.replace('/crearEquipo')
  // }

  return (
    <>
      {invoice ? (
        <MainContainer>
          <Invoice invoice={invoice}/>
          {/* <NextButton onClick={handleOnClick}>Aceptar</NextButton> */}
        </MainContainer>
      ):(
        <MainContainer>
          <h1>Cargando...</h1>
        </MainContainer>
      )}
    </>
  )
}
