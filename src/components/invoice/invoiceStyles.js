import styled from 'styled-components'

export const InvoiceContainer = styled.div`
  width: 90%;
  max-width: 500px;
  padding: 3rem 0 2rem;
  box-shadow: 1px 2px 5px rgba(0, 0, 0, 0.3);
  margin-bottom: 3rem;

  & h1 {
    margin: 0;
    margin-bottom: 2rem;
  }
`
export const InvoiceResume = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: ${props => props.border ? '1px solid #666': 'none'};
  padding: 1rem;
  align-items: center;
  margin-right: 1rem;
  margin-left: 1rem;
  margin-bottom: ${props =>  props.marginBottom ? '2rem' : '0'};

  & h2 {
    font-size: 1.2rem;
    margin: 0;
  }
  & p {
    margin: 0;
  }
`
