import Image from 'next/image';
import { BannerContainer, BannerTitle, HeroContainer } from './bannerStyles'
import hero from '../../../public/assets/banner3.jpeg';
import { PrimaryButton } from '../globalStyles';

export const Banner = () => {

  return (
    <BannerContainer>
      <HeroContainer>
        <Image
          src={hero}
          alt='Man writing on a keyboard.'
          layout='fill'
          objectFit='cover'
        />
      </HeroContainer>
      <BannerTitle>
        <h1>El espacio donde puedes encontrar una computadora a tu medida</h1>
        <PrimaryButton href='#information'>Consíguela</PrimaryButton>
      </BannerTitle>
    </BannerContainer>
  )
}
