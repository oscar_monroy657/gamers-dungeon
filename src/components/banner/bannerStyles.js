import styled from 'styled-components';
import device from '../../utils/css/breakpoints';

export const BannerContainer = styled.div`
  display: flex;
  height: 100vh;
  margin: 0;
  position: relative;
  overflow-x: hidden;

  @media ${device.mobileL} {
    margin-top: 104px;
    height: 100vh;
  }
  @media ${device.ipadPortrait} {
    height: calc(100vh - 104px);
  }
  @media ${device.laptop} {
    height: calc(100vh - 104px);
  }
`
export const HeroContainer = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
`

export const BannerTitle = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  background-color: rgba(0, 0, 0, .65);
  position: absolute;
  width: 100%;
  height: 100%;
  z-index: 10;
  right: 0;

  & h1 {
    font-family: 'Hammersmith One', sans-serif;
    text-align: center;
    color: #FFFFFF;
    font-size: 2.5rem;
    padding: .5rem;
    margin-bottom: 1rem;
  }

  @media ${device.mobileL} {
    align-items: flex-end;
    padding: 2rem;
    & h1 {
      text-align: end;
      font-size: 2rem;
      padding: 1rem;
    }
  }
  @media ${device.tablet} {
    & h1 {
      width: 60%;
      font-size: 2.5rem;
      line-height: 4rem;
    }
  }
  @media ${device.laptop} {
    & h1 {
      width: 60%;
      font-size: 3.25rem;
      line-height: 5rem;
      font-weight: normal;
    }
  }
  @media ${device.laptopM} {
    & h1 {
      font-size: 4.25rem;
    }
  }
`