import { useContext, useEffect, useState } from "react";
import { PayPalButtons } from "@paypal/react-paypal-js";
import { PaymentMessageContainer, PaypalButtonContainer } from "./paypalButtonStyles"
import CartContext from "../../context/cart/CartContext";

export const PaypalButton = ({ totalToPay, amountForPaypal, createInvoice, usdValue }) => {
  const [succeeded, setSucceeded] = useState(false);
	const [paypalErrorMessage, setPaypalErrorMessage] = useState("");
	const [orderID, setOrderID] = useState(false);
	const [billingDetails, setBillingDetails] = useState("");

	const { cartItems } = useContext(CartContext)
	const [newCartItems, setNewCartItems] = useState(cartItems)

	useEffect(() => {
		// console.log('Cart Cambiando');
		setNewCartItems(cartItems)
	}, [cartItems])

  // creates a paypal order
	const createOrder = (data, actions) => {
		// console.log('Paypal amount', amountForPaypal);
		return actions.order
			.create({
				purchase_units: [
					{
						description: "Compra de componentes para Pc.",
						amount: {
							value: amountForPaypal,
						},
					},
				],

				// remove the applicaiton_context object if you need your users to add a shipping address
				application_context: {
					shipping_preference: "NO_SHIPPING",
				},
			})
			.then((orderID) => {
				setOrderID(orderID);
				return orderID;
			});
	};

	// handles when a payment is confirmed by paypal
	const onApprove = (data, actions) => {
		return actions.order.capture().then(function (details) {
			const { payer, status } = details;
			// console.log(details, data);
			setBillingDetails(payer);
			setSucceeded(true);

      // console.log(status);
      if(status === 'COMPLETED'){
        createInvoice(newCartItems, amountForPaypal)
      }

		});
	};

	// handles when a payment is declined
	const onError = (data, actions) => {
		// console.log(data, actions);
		setPaypalErrorMessage("Something went wrong with your payment");
	};

  return (
		<div>
			<PaymentMessageContainer>
				<p>Para realizar los pagos con Paypal debemos convertir tu total a dólares.
					Por lo tanto, tu total de: <strong>Q{Number(totalToPay ?? 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</strong> será convertido a <strong>${Number(amountForPaypal ?? 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</strong>.
				</p>
				<p>Tasa de cambio: Q1 = <strong>${usdValue}</strong></p>
			</PaymentMessageContainer>
			<PaypalButtonContainer>
				<PayPalButtons
						style={{
							color: "blue",
							shape: "pill",
							label: "pay",
							tagline: false,
							layout: "vertical",
						}}
						createOrder={createOrder}
						onApprove={onApprove}
						onError={onError}
					/>

					{/* Show an error message on the screen if payment fails */}
					{paypalErrorMessage && (
						<p>{paypalErrorMessage}</p>
					)}

					{/* <p>
						Payer details will show up below once payment is completed:
					</p>

					{succeeded && (
						<section>
							{billingDetails && (
								<div>
									<pre>{JSON.stringify(billingDetails, undefined, 2)}</pre>
								</div>
							)}
						</section>
					)} */}
			</PaypalButtonContainer>
		</div>
  )
}
