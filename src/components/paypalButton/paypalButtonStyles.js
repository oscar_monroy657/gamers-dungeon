import styled from 'styled-components'
import device from '../../utils/css/breakpoints'

export const PaypalButtonContainer = styled.div`
  width: 100%;
  margin-left: auto;
  margin-right: auto;

  @media ${device.tablet}{
    width: 75%;
  }
`

export const PaymentMessageContainer = styled.div`
  background-color: #FFF3CD;
  border: 1px solid #FFEEBA;
  color: #856404;
  padding: 1rem;
  margin-bottom: 1rem;
  border-radius: .3rem;
  text-align: left;

  & p {
    line-height: 1.3rem;

    & strong {
      color: #403003;
    }
  }
`