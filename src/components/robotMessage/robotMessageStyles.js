import styled, { keyframes } from 'styled-components';

const rotate = keyframes`
  0% {
    transform: rotate(0deg);
  }

  15%{
    transform: rotate(25deg);
  }
  30%{
    transform: rotate(45deg);
  }
  45%{
    transform: rotate(25deg);
  }
  60%{
    transform: rotate(0deg);
  }
  75%{
    transform: rotate(-25deg);
  }
  100% {
    transform: rotate(0deg);
  }
`;

export const RobotContainer = styled.div`
	width: 100%;
	display: flex;
	justify-content: center;
	align-items: center;
  position: relative;
  flex-wrap: wrap;
  margin-bottom: 1rem;
`;
export const RobotMessageContainer = styled.div`
	width: 100%;
	position: relative;
	display: flex;
	justify-content: center;
	align-items: center;
`;
export const RobotIconContainer = styled.div`
	width: 3rem;
	height: 3rem;
  animation: ${rotate} 2s linear;
  animation-iteration-count: 2;
  margin-left: ${props => props.ml ? '1rem': '0'};
  margin-top: ${props => props.mt ? '1rem': '0'};
  cursor: pointer;
  position: relative;

  &::after {
    content: '';
    display: block;
    position: absolute;
    background-image: url('/assets/information.svg');
    width: 2rem;
    height: 2rem;
    left: 100%;
    top: -25%;
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
  }

	& img {
		width: 100%;
		height: 100%;
	}
`;