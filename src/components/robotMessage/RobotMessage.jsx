import React from "react";
import { RobotIconContainer, RobotMessageContainer } from "./robotMessageStyles";
import { Modal } from '../modal/Modal';

export const RobotMessage = ({isOpen, closeModal, message, url, noShowLink}) => {

	return (
		<>
			<Modal isOpen={isOpen} closeModalHandle={closeModal} title={'Información'} forOrden={false}>
				<RobotMessageContainer>
					<RobotIconContainer mt>
						<img src="/assets/bot.svg" alt="asistente online" />
					</RobotIconContainer>
				</RobotMessageContainer>
				<div style={{ textAlign: 'left'}}>
					{message}
					<br />
					{/* {!noShowLink && (
						<span>
							Para más información puedes dar click en este <span style={{ marginBottom: '.3rem' }}>👉🏼</span> <a href={url} target="_blank" style={{ color: '#0c3b69', fontWeight: '700'}}>enlace</a>
						</span>
					)} */}
				</div>
			</Modal>
		</>
	);
};