import styled from 'styled-components';
import device from '../utils/css/breakpoints';

export const MainContainer = styled.div`
	width: 100%;
	margin: 0;
	position: relative;
	overflow-x: hidden;
  overflow-y: clip;
  display: flex;
  justify-content: flex-start;
  flex-direction: column;
  min-height: calc(100vh - 155px);
  padding-top: 7rem;

  @media ${device.mobileL} {
    min-height: 100vh;
  }
  @media ${device.ipadPortrait} {
    min-height: calc(100vh - 104px);
    justify-content: center;
  }
  @media ${device.laptop} {
    min-height: calc(100vh - 104px);
  }
`;

export const ContentContainer = styled.div`
	width: 90%;
	margin: 0 auto;
  margin-bottom: 2rem;

	@media ${device.mobileL} {
    width: 65%;
    margin: 0;
    margin-bottom: 2rem;
    margin-left: 1rem;
  }
  @media ${device.ipadPortrait} {
    width: 65%;
    margin-left: 2rem;
  }
  @media ${device.laptop} {
    width: 50%;
    margin-left: 5rem;
  }
  @media ${device.laptopM} {
    
  }
`

export const ContentTitle = styled.h2`
  color: #c91817;
`
export const ContentDescription = styled.p`
  color: #333;
  margin-bottom: 1rem;
`

export const CornerImage = styled.div`
  position: absolute;
  width: 255.84px;
  height: 100%;
  right: 0;
  top: 0;
	display: none;

	@media ${device.mobileL} {
		display: block;
    z-index: -1;
    right: 0;
    height: 211vh;
    width: 38%;
    top: -4rem
	}

	@media ${device.mobileSLandscape}{
		right: -4rem;
    top: -7rem;
	}

	@media ${device.tablet}{
		width: 38%;
	}

	@media ${device.ipadPortrait} {
    top: -15rem;
    width: 68%;
    right: -12rem;
    height: 115vh;
  }
	@media ${device.ipadImage}{
		right: 0;
	}
	@media ${device.laptopM}{
    height: 130vh;
		right: 0;
		top: -10rem;
		width: 30%;
	}
`

export const LoginPageContainer = styled.div`
  display: flex;
  min-height: 100vh;
  justify-content: space-around;
  align-items: center;
  margin-top: 52px;
`;

export const LoginCover = styled.div`
  display: none;
  justify-content: center;
  & img {
    width: 75%;
  }
  @media ${device.tablet} {
    display: flex;
    width: 40%;
  }
  @media ${device.laptop} {
    display: flex;
    width: 35%;
  }
`
export const MainSpinnerContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  min-height: ${ props => props.height ? props.height + 'vh' : '100vh'};
  margin-top: 104px;
`
export const PrimaryButton = styled.a`
  border: none;
  background-color: #303030;
  color: #FFFFFF;
  outline: none;
  padding-top: ${(props) => props.services ? '.75rem': '1rem'};
  padding-bottom: ${(props) => props.services ? '.75rem': '1rem'};
  padding-left: ${(props) => props.services ? '2rem': '1rem'};
  padding-right: ${(props) => props.services ? '2rem': '1rem'};
  border-radius: 8px;
  cursor: pointer;
  font-size: ${(props) => props.services ? '1rem': '1.3rem'};
  font-weight: 700;
`

export const ButtonToTopContainer = styled.button`
  width: 50px;
  height: 50px;
  border-radius: 50%;
  position: fixed;
  right: 1rem;
  bottom: 2rem;
  border: none;
  background: #333;
  color: #FFFFFF;
  cursor: pointer;
  display: flex;
  justify-content: center;
  align-items: center;
  z-index: 1000;

  & img {
    width: 75%;
    height: 75%;
  }
`

export const ViewMoreLink = styled.a`
  border: none;
  cursor: pointer;
  padding: .7rem 3rem;
  border-radius: .5rem;
  font-size: 1rem;
  font-weight: 700;
  background-color: #303030;
  color: #FFFFFF;
  margin-right: 1rem;
  margin-bottom: 1rem;
`