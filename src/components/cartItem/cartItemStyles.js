import styled from "styled-components";
import device from "../../utils/css/breakpoints";

export const CartItemContainer = styled.div`
	display: flex;
	box-shadow: 0 0 2px rgba(0, 0, 0, 0.3);
	flex-direction: column;
  margin-bottom: 2rem;

	@media ${device.tablet} {
		flex-direction: row;
	}
`;

export const CartImageContainer = styled.div`
	width: 100%;

	& img {
		width: 100%;
		height: 100%;
	}

  @media ${device.tablet}{
    width: 35%;
  }
  @media ${device.laptop}{
    width: 40%;
  }
`;

export const CartBody = styled.div`
	width: 100%;
	display: flex;
	flex-direction: column;
  align-items: flex-end;
  padding: 1rem 1rem 0;

  @media ${device.tablet}{
    width: 65%;
  }
  @media ${device.laptop}{
    width: 60%;
  }
`;

export const CartBodyTitle = styled.h2`
  font-size: 1.25rem !important;
  width: 100%;
  /* white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden; */
  text-align: center;
  margin: 0;
  margin-bottom: 1rem;
`

export const CartItemInfoContainer = styled.div`
	display: flex;
  margin-right: .8rem;
  margin-bottom: 1rem;
`;

export const CartInfoText = styled.p`
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: center;
	& img {
		display: block;
		width: 2rem;
		margin: 0 auto;
		cursor: pointer;
	}
`;

export const CartQuantityContainer = styled.div`
	display: flex;
  justify-content: flex-end;
	align-items: center;
	margin-right: 1rem;
	margin-left: 2rem;

	& p {
		width: 2rem;
    height: 1.75rem;
    border: 1px solid rgba(0,0,0, .3);
	}
`;
export const QuantityButton = styled.button`
	width: 1.75rem;
	height: 1.75rem;
	border: none;
	background-color: white;
	cursor: pointer;
  border: 1px solid rgba(0,0,0, .3);
  border-right: ${props => props.left ? 'none' : '1px solid rgba(0,0,0, .3)'};
  border-left: ${props => props.left ? '1px solid rgba(0,0,0, .3)' : 'none'};

	& img {
		width: 100%;
		height: 100%;
	}
`;

export const DeleteButton = styled.button`
  width: 100%;
  border: none;
  background-color: #E53935;
  color: white;
  padding: 1rem;
  font-size: 1rem;
  font-weight: 700;
  cursor: pointer;
  transition: background-color .3s ease-in-out;
  border-radius: .8rem;
  margin-bottom: 1rem;

  @media ${device.tablet}{
    width: 60%;
  }

  &:hover {
    background-color: #AB000D;
  }
`