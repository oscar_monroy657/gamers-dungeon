import { CartItemInfoContainer, CartItemContainer, CartQuantityContainer, QuantityButton, CartBody, CartImageContainer, CartInfoText, CartBodyTitle, DeleteButton } from "./cartItemStyles";

export const CartItem = ({ product, deleteProduct, addProduct, removeProduct }) => {
	const { quantity } = product;
	const price = product.precio || product.precio_producto
	const name = product.nombre_producto || product.nombre
	const total = price * quantity

	return (
		<CartItemContainer>
			<CartImageContainer>
				<img src={product.img_url || product.url} alt={name} />
			</CartImageContainer>
			<CartBody>
					<CartBodyTitle>{name}</CartBodyTitle>
				<CartItemInfoContainer>
					<CartQuantityContainer>
						{quantity > 1 && <QuantityButton left type='button' onClick={() => removeProduct(product)}>
							<img src="/assets/minus.svg" alt="minus one product" />
						</QuantityButton>}
						<CartInfoText>{quantity}</CartInfoText>
						<QuantityButton type='button' onClick={() => addProduct(product)}>
							<img src="/assets/plus.svg" alt="plus one product" />
						</QuantityButton>
					</CartQuantityContainer>
					<CartInfoText>Q {Number(price ?? 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</CartInfoText>
				</CartItemInfoContainer>
				<CartItemInfoContainer>
					<p>Q {Number(total).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</p>
				</CartItemInfoContainer>
				<DeleteButton
					type='button'
					onClick={() => deleteProduct(product)}
				>Eliminar</DeleteButton>
			</CartBody>
		</CartItemContainer>
	);
};
