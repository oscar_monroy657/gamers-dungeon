import { useRouter } from 'next/router'
import { NextButton } from '../assistent/assistentStyles'
import { EmptyCartIconContainer } from './emptyCartStyles'

export const EmptyCart = () => {
  const router = useRouter()

  const handleOnClick = () => {
    router.push('/productos')
  }
  return (
    <EmptyCartIconContainer>
      <img src='/assets/empty_cart.svg' allt='Sin productos en el carrito.'/>
      <h2>No tienes productos.</h2>
      <NextButton onClick={handleOnClick}>Ver productos</NextButton>
    </EmptyCartIconContainer>
  )
}
