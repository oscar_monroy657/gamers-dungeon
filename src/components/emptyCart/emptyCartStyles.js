import styled from "styled-components";
import device from "../../utils/css/breakpoints";

export const EmptyCartIconContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 80%;
  & img {
    width: 75%;
  }
  @media ${device.tablet} {
    width: 40%;
  }
  @media ${device.laptop} {
    display: flex;
    width: 35%;
  }
`