import { useRouter } from 'next/router'
import { useContext } from 'react'
import CartContext from '../../context/cart/CartContext'
import RobotContext from '../../context/robot/RobotContext';
import { CartCount, CartIconContainer, Icon } from './cartIconStyles'

export const CartIcon = () => {
  const { cartItemsCount } = useContext(CartContext)
  const { setPageToShow } = useContext(RobotContext)
  const router = useRouter()

  return (
    <CartIconContainer onClick={() => {
      if(cartItemsCount < 1) return
      setPageToShow('/checkout')
      router.push('/checkout')
    }}>
      <Icon src='/assets/shopping-cart.svg' alt='shopping cart'/>
      {cartItemsCount >= 1 && <CartCount>{cartItemsCount}</CartCount>}
    </CartIconContainer>
  )
}
