import styled from 'styled-components'

export const CartIconContainer = styled.div`
  width: 45px;
  height: 45px;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
`
export const Icon = styled.img`
  width: 30px;
  height: 30px;
`
export const CartCount = styled.span`
  position: absolute;
  font-size: .7rem;
  font-weight: bold;
  bottom: 35px;
  color: #000000;
  display: flex;
  background-color: #FFFFFF;
  border-radius: 50%;
  width: 25px;
  height: 25px;
  align-items: center;
  justify-content: center;
`