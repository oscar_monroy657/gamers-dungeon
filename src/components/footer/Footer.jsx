import Image from "next/image";
import { FooterContainer } from "./footerStyles";

export const Footer = () => {
	return (
		<FooterContainer>
			<p>Síguenos en:</p>
			<div>
				<a href="https://www.instagram.com/gamersdungeon01012" target="_blank">
					<Image
						src="/assets/instagram.svg"
						alt="Instagram"
						layout="fixed"
						width={50}
						height={30}
					/>
				</a>
				<a href="https://discord.gg/GM29sNwXJX" target="_blank">
					<Image
						src="/assets/discord3.svg"
						alt="Discord"
						layout="fixed"
						width={50}
						height={30}
					/>
				</a>
				<a href="https://wa.me/50254490185/?text=Hola,%20necesito%20más%20información." target="_blank">
					<Image
						src="/assets/whatsapp.svg"
						alt="Discord"
						layout="fixed"
						width={50}
						height={30}
					/>
				</a>
				<a href="https://www.tiktok.com/@gamersdungeon?lang=es" target="_blank">
					<Image
						src="/assets/tik-tok.svg"
						alt="Tik tok"
						layout="fixed"
						width={50}
						height={30}
					/>
				</a>
				<a href="https://www.facebook.com/Gamers-Dungeon-102213178847121" target="_blank">
					<Image
						src="/assets/facebook.svg"
						alt="Facebook"
						layout="fixed"
						width={50}
						height={30}
					/>
				</a>
				<a href="https://www.youtube.com/channel/UCMv69-a3NrAa5mk_tpC2Alg" target="_blank">
					<Image
						src="/assets/youtube.svg"
						alt="Youtube"
						layout="fixed"
						width={50}
						height={30}
					/>
				</a>
			</div>
		</FooterContainer>
	);
};
