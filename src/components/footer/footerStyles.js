import styled from 'styled-components'

export const FooterContainer = styled.footer`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background-color: #202020;
  padding: 2rem 0;
  margin-top: 2rem;

  & p{
    color: #FFFFFF;
    font-size: 1.2rem;
  }
`

export const FooterLink = styled.a`
  display: inline-block;
  width: 50px;
  height: 30px;

  & img {
    width: 100%;
    height: 100%;
  }
`