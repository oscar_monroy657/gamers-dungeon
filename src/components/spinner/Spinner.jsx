import { HalfSpinner, CircleOne, CircleTwo } from './spinnerStyles';

export const Spinner = ({ size, color='#FFFFFF' }) => {
  return (
    <HalfSpinner spinnerColor={color} spinnerSize={size}>
      <CircleOne spinnerColor={color} spinnerSize={size}></CircleOne>
      <CircleTwo spinnerColor={color} spinnerSize={size}></CircleTwo>
    </HalfSpinner>
  )
}