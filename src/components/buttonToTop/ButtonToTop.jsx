import { ButtonToTopContainer } from "../globalStyles";

export const ButtonToTop = () => (
  <ButtonToTopContainer onClick={() => window.scrollTo({ top: 0, left: 0, behavior: 'smooth' })}>
    <img src="/assets/top.svg" alt="Button to top" />
  </ButtonToTopContainer>
)
