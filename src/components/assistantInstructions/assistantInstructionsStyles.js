import styled from 'styled-components'
import device from '../../utils/css/breakpoints'

export const AssistantInstructionsContainer = styled.div`
  display: flex;
  min-height: 100vh;
  justify-content: space-around;

	& h2 {
		margin-bottom: 1rem;
	}

`
export const AssistantInstructionsContent = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-start;
	color: #607D8B;
	width: 90%;

	& p, ul {
		text-align: left;
	}

	@media ${device.mobileL} {
		width: 50%;
	}
	@media ${device.laptop} {
		width: 45%;
	}
`
export const AssistantInstructionsImage = styled.div``