import { useContext, useState } from "react"
import AppContext from "../../context/AppContext"
import { NextButton } from "../assistent/assistentStyles"
import { LoginCover } from "../globalStyles"
import { PaymentMessageContainer } from "../paypalButton/paypalButtonStyles"
import { AssistantInstructionsContainer, AssistantInstructionsContent } from "./assistantInstructionsStyles"
import styles from './checkboxTemp.module.css'

export const AssitantInstructions = ({ changeStep }) => {
  const [showMessageAgain, setShowMessageAgain] = useState(true)
  const { setInitialStepForBuildComplete } = useContext(AppContext)

  const handleClick = () => {
    if(!showMessageAgain){
      // const showLS = JSON.stringify(showMessageAgain)
      localStorage.setItem('showInstructions', 'No show')
      setInitialStepForBuildComplete(1)
    }
    changeStep(1)
  }

  const handleLabelClick = () => {
    setShowMessageAgain(!showMessageAgain)
  }

  return (
    <AssistantInstructionsContainer>
      <AssistantInstructionsContent>
        <h2>Bienvenido al asistente.</h2>
        <p>A continuación te mostraremos opciones para que elijas cosas como:</p>
        <ul>
          <li>El sistema operativo.</li>
          <li>Las actividades que realizarás con la computadora.</li>
          <li>Tu presupuesto.</li>
          <li>Etc.</li>
        </ul>
        <p>Estos datos le permitirán al asistente elegir los componentes que mejor se adapten a tus necesidades.</p>
        <PaymentMessageContainer>
          <h3>Atención.</h3>
          <p>En algunos casos te mostraremos dos opciones de componentes (dos tarjetas madre, dos procesadores, dos escritorios, etc), esto es porque dichos componentes se adaptan a tus necesidades, pero te damos la opción para que tú elijas con cuál te quedas según tu presupuesto. <strong>Recuerda: ¡Cualquier componente que elijas será el adecuado!</strong></p>
          <p>Podrás eliminar o aumentar la cantidad de algún producto en la página del carrito.</p>
        </PaymentMessageContainer>
        <p>Si tienes alguna duda ¡contáctanos! Puedes hacerlo a tráves de la opción "Contacto" o puedes seguirnos en nuestras redes sociales que encontrarás al final de la página.</p>
        <div className={styles.chekboxContainer}>
          <input type="checkbox" name="showAgain" id='showAgain' className={styles.checkbox}/>
          <label htmlFor="showAgain" onClick={handleLabelClick}>No mostrar este mensaje de nuevo.</label>
        </div>
        <NextButton onClick={handleClick}>Empezar</NextButton>
      </AssistantInstructionsContent>
      <LoginCover>
        <img src="/assets/instructions.svg" alt="" />
      </LoginCover>
    </AssistantInstructionsContainer>
  )
}
