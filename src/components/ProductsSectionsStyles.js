import styled, { css } from "styled-components";

const titlesStyles = css`
  text-align: center;
`

export const MainTitleSection = styled.h2`
  font-size: 2rem;
  ${titlesStyles}
`
export const TitleSection = styled.h3`
  font-size: 1.7rem;
  margin-bottom: 2rem;
  ${titlesStyles}
`