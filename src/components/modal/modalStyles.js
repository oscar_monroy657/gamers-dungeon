import styled from 'styled-components'
import device from '../../utils/css/breakpoints'

export const ModalComponent = styled.div`
  position: fixed;
  z-index: 11;
  top: 26px;
  left: 0;
  width: 100%;
  min-height: 100vh;
  background-color: rgba(0, 0, 0, .65);
  display: ${props => props.isOpen ? 'flex' : 'none'};
  justify-content: center;
  align-items: center;
  transition: display 1s ease-in-out;

  @media ${device.tablet}{
    top: 52px;
  }
`
export const ModalContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  padding: 1rem 1rem 2rem;
  width: 95%;
  min-height: 200px;
  max-height: 600px;
  background-color: #FFFFFF;
  box-shadow: 0 3px 11px -5px rgba(0,0,0, .3);
  border-radius: .5rem;

  @media ${device.tablet}{
    width: 500px;
  }
`
export const ModalTitle = styled.h2`
  flex: 2;
  margin-top: 0;
  margin-bottom: 1rem;
  text-align: left;
  padding: 1rem;
  border-bottom: 1px solid rgba(0,0,0,.2);
`
export const ModalContentContainer = styled.div`
  flex: 1;
  overflow-y: auto;
  margin-bottom: 1rem;
`

export const ModalCloseButtonContainer = styled.div`
  width: 100%;
  position: absolute;
  padding: 1rem;
  bottom: 0;
  left: 0;
  background-color: #FFFFFF;
`
export const ModalCloseButton = styled.button`
  flex: 2;
  border: 1px solid #1976D2;
  background-color: #FFFFFF;
  border-radius: .2rem;
  color: #1976D2;
  cursor: pointer;
  font-size: 1rem;
  padding: .5rem;
  transition: background-color .3s ease-in-out;
  width: 95%;
  margin-left: auto;
  margin-right: auto;

  &:hover {
    background-color: rgba(25, 118, 210, .1);
  }

  @media ${device.tablet}{
    width: 60%;
  }
`