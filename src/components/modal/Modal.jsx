import { ModalCloseButton, ModalComponent, ModalContainer, ModalContentContainer, ModalTitle } from "./modalStyles"

export const Modal = ({ children, isOpen, closeModalHandle, title, forOrden = true }) => {
  const handleModalContainerClick = e => e.stopPropagation()

  return (
    <ModalComponent onClick={closeModalHandle} isOpen={isOpen}>
      <ModalContainer onClick={handleModalContainerClick}>
        {forOrden && <ModalTitle>Detalles de orden: {title}</ModalTitle>}
        <ModalContentContainer>
          {children}
        </ModalContentContainer>
        <ModalCloseButton type='button' onClick={closeModalHandle}>Cerrar</ModalCloseButton>
      </ModalContainer>
    </ModalComponent>
  )
}
