import { useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { FormContainer, FormHeader, InputLabel, Input, SubmitButton, FormLink, GoogleButton, ErrorMessageContainer } from '../formStyles'
import { Spinner } from '../../spinner/Spinner'
import AppContext from '../../../context/AppContext'
import RobotContext from '../../../context/robot/RobotContext';
import * as gtag from '../../../../utils/gtag'

export const LoginForm = () => {
  const [userCredentials, setUserCredentials] = useState({email: '', password: ''})
  const { user, isLoading, signInUser, signInWithGoogleAccount, error, message } = useContext(AppContext)
  const { pageToShow } = useContext(RobotContext)
  const router = useRouter()
  const { email, password } = userCredentials

  useEffect(() => {
    if(user){
      router.replace(pageToShow)
    }
  }, [user])

  const handleOnChange = (e) => {
    const { name, value } = e.target
    setUserCredentials({
      ...userCredentials,
      [name]: value,
    })
  }

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if(email.trim() === '' || password.trim() === ''){
      return
    }
    gtag.event({
      action: 'login_with_form',
      category: 'user_activity',
      label: 'Login Form',
      value: 'Login by form'
    })
    signInUser(email, password)
  }

  return (
    <FormContainer>
      <FormHeader centered>
        <h1>Bienvenido</h1>
      </FormHeader>
      <form onSubmit={handleOnSubmit}>
        <InputLabel fullWidth>
          <span>Correo: </span>
          <Input 
            type='email' 
            placeholder='Correo electrónico'
            name='email'
            value={email}
            onChange={handleOnChange}
            required
          />
        </InputLabel>
        <InputLabel fullWidth>
          <span>Contraseña: </span>
          <Input 
            type='password' 
            placeholder='Contraseña'
            name='password'
            value={password}
            onChange={handleOnChange}
            required
          />
        </InputLabel>
        {error &&  (
          <ErrorMessageContainer>
            <p>{message}</p>
          </ErrorMessageContainer>
        )}
        <SubmitButton type='submit'>
          {isLoading ? <Spinner size='20' color='#FFFFFF'/> : 'Ingresar'}
        </SubmitButton>
      </form>
      <p>O</p>
      <GoogleButton type="button" onClick={() => {
        gtag.event({
          action: 'login_with_google',
          category: 'user_activity',
          label: 'Google Button',
          value: 'Login by google'
        })
        signInWithGoogleAccount()
      }}>
        <img src="/assets/google.svg" alt="Google signin" width={16} height={16}/>
        Ingresa con Google
      </GoogleButton>
      <p>¿No tienes una cuenta? <FormLink href='/signup'><a> Crear cuenta</a></FormLink> </p>
    </FormContainer>
  )
}
