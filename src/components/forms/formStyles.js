import styled, { css } from 'styled-components';
import device from '../../utils/css/breakpoints';
import Link from 'next/link'

export const CreateFormContainer = styled.div`
	width: 85%;
	margin: 0 auto;
	& form {
		display: flex;
		flex-direction: column;
		flex-wrap: wrap;
		justify-content: space-between;
		align-items: center;
	}
	@media ${device.mobileL} {
		& form {
			flex-direction: row;
		}
	}
`;

export const FormContainer = styled.div`
	display: flex;
	flex-direction: column;
	width: 85%;
	& p {
		text-align: center;
	}
	@media ${device.mobileL} {
		width: 40%;
	}
	@media ${device.laptop} {
		width: 35%;
	}
`;

export const FormHeader = styled.div`
	display: flex;
	justify-content: ${(props) => (props.centered ? 'center' :  'flex-start')};
	align-items: center;
	margin-bottom: 1rem;

	& img {
		width: 2rem;
		height: 2rem;
		object-fit: cover;
		margin-right: 1rem;
	}
`;

export const InputLabel = styled.label`
	display: flex;
	flex-direction: column;
	font-weight: 700;
	font-size: 1.2rem;
	margin-bottom: 2rem;
	width: 100%;
	& span {
		margin-bottom: 0.75rem;
		color: #303030;
	}
	@media ${device.mobileL} {
		width: ${(props) => (props.fullWidth ? '100%' : '45%')};
	}
`;

const inputStyles = css`
	padding: 0.5rem 0;
	border: none;
	border-bottom: 2px solid #cdcfd3;
	color: #a5a7ad;
	font-size: 1.1rem;
	font-weight: 700;
	line-height: 1.5rem;
	transition: border 1s ease-in-out;
	&:focus {
		outline: none;
		border-color: #303030;
	}
`;
export const Input = styled.input`
	${inputStyles};
`;
export const TextArea = styled.textarea`
	${inputStyles}
	&::-webkit-scrollbar {
		width: 10px;
	}
	&::-webkit-scrollbar-thumb {
		background: #b3b3b3;
		border-radius: 4px;
	}
	&::-webkit-scrollbar-track {
		background: #e1e1e1;
		border-radius: 4px;
	}
`;

export const Select = styled.select`
	display: block;
  width: 100%;
  padding: 1rem;
  border: 1px solid #e1e1e1;
  appearance: none;
	transition: border-color 1s ease-in-out;
	color: #a5a7ad;
	font-size: 1rem;
	font-weight: 700;
	outline: none;
	&:hover, &:focus {
		border-color: #001557;
	}
	& option {
		text-transform: capitalize;
	}
`

export const ButtonsContainer = styled.div`
	display: flex;
	flex-direction: column;
	justify-content: space-between;
	align-items: center;
	width: 100%;
	@media ${device.mobileL} {
		flex-direction: row;
		& button {
			width: 45%;
		}
	}
`;

const buttonStyles = css`
	cursor: pointer;
	border-radius: 0.75rem;
	border: none;
	display: block;
	font-size: 1rem;
	font-weight: 700;
	width: 100%;
	padding: 0.75rem 0.5rem;
`

export const SubmitButton = styled.button`
	background-color: #303030;
	color: #ffffff;
	${buttonStyles}
	transition: background-color 0.7s ease-in-out;
`;

export const GoogleButton = styled.button`
	${buttonStyles}
	display: flex;
	justify-content: center;
	align-items: center;
	background-color: #FFFFFF;
	color: #303030;
	box-shadow: 1px 0 5px 0 rgba(0,0,0, .3);
	margin-bottom: 1rem;
	font-family: 'Roboto', sans-serif;

	& img {
		margin-right: .5rem;
	}
`

export const FormLink = styled(Link)`
	text-decoration: none;
	color: #560015;
	font-size: 1rem;
	font-weight: 700;
`;
export const Notice = styled.p`
	color: #560015;
	font-size: 1rem;
	text-align: center;
`;

export const ErrorMessageContainer = styled.div`
  background-color: #f8d7da;
  border: 1px solid #f5c6cb;
  color: #721c24;
  padding: .5rem 1rem;
  margin-bottom: 1rem;
  border-radius: .3rem;
  text-align: left;

  & p {
    line-height: 1.3rem;

    & strong {
      color: #403003;
    }
  }
`