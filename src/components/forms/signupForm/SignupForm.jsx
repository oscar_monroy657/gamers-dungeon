import { useContext, useEffect, useState } from 'react'
import { useRouter } from 'next/router'
import { FormContainer, FormHeader, InputLabel, Input, SubmitButton, FormLink, ErrorMessageContainer } from '../formStyles'
import { Spinner } from '../../spinner/Spinner'
import { initialSignupUser } from './userState'
import AppContext from '../../../context/AppContext'
import * as gtag from '../../../../utils/gtag'

export const SignupForm = () => {

  const [userCredentials, setUserCredentials] = useState(initialSignupUser)
  const { user, isLoading, registerUser, error, message } = useContext(AppContext)
  const router = useRouter()
  const { email, password, confirmPassword, displayName } = userCredentials

  useEffect(() => {
    if(user){
      router.replace('/crearEquipo')
    }
  }, [user])

  const handleOnChange = (e) => {
    const { name, value } = e.target;
    setUserCredentials({
      ...userCredentials,
      [name]: value
    })
  }

  const handleOnSubmit = (e) => {
    e.preventDefault();
    if(email.trim() === '' || password.trim() === '' || confirmPassword.trim() === '' || displayName.trim() === ''){
      // console.log('Campos obligatorios');
      return;
    }
    if(password.trim() !== confirmPassword.trim()){
      // console.log('Contraseñas no iguales');
      return;
    }
    gtag.event({
      action: 'signup_action',
      category: 'user_activity',
      label: 'Signup Form',
    })
    registerUser(email, password, { displayName })
  }

  return (
    <FormContainer>
      <FormHeader centered>
        {/* <img src="/assets/graph_bar.svg" alt="abc business logo" /> */}
        <h1>Crea una cuenta</h1>
      </FormHeader>
      <form onSubmit={handleOnSubmit}>
        <InputLabel fullWidth>
          <span>Nombre: </span>
          <Input 
            type='text' 
            placeholder='Nombre' 
            value={displayName} 
            name='displayName'
            onChange={handleOnChange}
            required
          />
        </InputLabel>
        <InputLabel fullWidth>
          <span>Correo: </span>
          <Input 
            type='email' 
            placeholder='Correo electrónico' 
            value={email}
            name='email'
            onChange={handleOnChange}
            required
          />
        </InputLabel>
        <InputLabel fullWidth>
          <span>Contraseña: </span>
          <Input 
            type='password' 
            placeholder='Contraseña'
            value={password}
            name='password'
            onChange={handleOnChange}
            required
          />
        </InputLabel>
        <InputLabel fullWidth>
          <span>Confirmar contraseña: </span>
          <Input 
            type='password' 
            placeholder='Confirmar contraseña'
            value={confirmPassword}
            name='confirmPassword'
            onChange={handleOnChange}
            required
          />
        </InputLabel>
        {error &&  (
          <ErrorMessageContainer>
            <p>{message}</p>
          </ErrorMessageContainer>
        )}
        <SubmitButton type='submit'>
          {isLoading ? <Spinner size='20' color='#FFFFFF'/> : 'Crear cuenta'}
        </SubmitButton>
      </form>
      <p>¿Ya tienes una cuenta? <FormLink href='/login'><a>Ingresar</a></FormLink> </p>
    </FormContainer>
  )
}
