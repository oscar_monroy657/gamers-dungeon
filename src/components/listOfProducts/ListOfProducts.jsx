import { ProductsContainer, TotalQuiz } from './listProductsStyles'
import { ProductItem } from './ProductItem'

export const ListOfProducts = ({products}) => {
  const totalToPay = products.reduce((total, row) => total + Number(row.total), 0)

  return (
    <>
      <ProductsContainer>
        {products.map(product => (
            <>
              {product.details.map(detail => (
                <ProductItem product={detail} key={detail.codigo_producto}/>
              ))}
            </>
        ))}
      </ProductsContainer>
      <TotalQuiz>Total a pagar: <strong>Q{Number(totalToPay ?? 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</strong></TotalQuiz>
    </>
  )
}
