import { Item, ItemImage, ItemTitle } from "./productItemStyles"

export const ProductItem = ({product}) => {
  return (
    <Item>
      <ItemImage>
        <img src={product.url} alt={product.nombre}/>
      </ItemImage>
      <ItemTitle>{product.nombre}</ItemTitle>
      <p>Precio: <strong>Q{Number(product.precio ?? 0).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ",")}</strong></p>
    </Item>
  )
}
