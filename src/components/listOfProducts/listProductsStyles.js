import styled from 'styled-components'
import device from "../../utils/css/breakpoints";

export const ProductsContainer = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  width: 95%;
  margin: 0 auto;

  @media ${device.mobileL}{
    flex-direction: row;
    justify-content: center;
  }
`
export const GroupedProductsContainer = styled.div`
  width: 100%;
  margin-bottom: 3rem;
  text-align: center;

  & > p {
    font-size: 1.5rem;
  }
`

export const ProductsTitle = styled.h1`
  font-size: 1.5rem;
  margin-bottom: 0;
`
export const ProductSubtitle = styled.p`
  font-size: 1rem;
  color: #666;
  margin-bottom: 3rem;
`

export const TotalQuiz = styled.p`
  margin-bottom: 3rem;
`