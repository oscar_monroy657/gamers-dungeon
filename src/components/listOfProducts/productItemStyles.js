import styled from "styled-components";
import device from "../../utils/css/breakpoints";

export const Item = styled.div`
  box-shadow: 2px 3px 9px -5px rgba(0 0 0 / 50%);
  width: 100%;
  margin-bottom: 2.5rem;
  text-align: center;

  @media ${device.mobileL}{
    margin-right: 1rem;
    width: 30%;
  }
`

export const ItemImage = styled.div`
  width: 100%;
  height: 250px;

  & img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    object-position: center;
  }
`
export const ItemTitle = styled.h2`
  font-size: 1rem !important;
  color: #666;
  text-align: center;
  padding: 0 1rem;
`
export const ItemDescription = styled.p`
  font-size: .75rem;
`

export const ListItem = styled.li`
  text-align: left;
  margin-bottom: 1rem;
  color: #34515E;
  margin-right: 1rem;
`
export const ListItemFooter = styled.div`
  text-align: center;
  width: 100%;
  padding-left: 40px;
  margin-bottom: 1rem;
`