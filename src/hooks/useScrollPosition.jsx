import { useEffect, useRef, useState } from 'react'

export const useScrollPosition = () => {
  const [scrollPosition, setScrollPosition] = useState(0)
  const isMounted = useRef(true)

  useEffect(() => {
    return () => {
      isMounted.current = false
    }
  }, [])

  useEffect(() => {
    window.onscroll = () => {
      if(isMounted.current){
        setScrollPosition(window.pageYOffset)
      }
    }
  }, [])

  return scrollPosition
}
