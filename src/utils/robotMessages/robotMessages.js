import { ListItem } from '../../components/listOfProducts/productItemStyles';
export const robotMessages = {
	operating_systems: {
		message: (name) => (
			<span>
				<span
					style={{
						display: "block",
						textAlign: "center",
						marginBottom: "1rem",
					}}
				>
					Hola {name}, ¡soy <strong>Desky</strong>!
				</span>{" "}
				En este apartado elegirás el sistema operativo con el que quieres
				trabajar. Son importantes porque te permiten interactuar y darle órdenes
				al computador 💻.
				<br />
				<br />
				Sin el sistema operativo, no tendrías la plataforma que soporta los
				programas que te permiten hacer cartas, escuchar música, navegar por
				internet o enviar un correo electrónico 😯. <br /> <br />
				💡 Los dos sistemas operativos más populares son{" "}
				<strong>Windows y Linux</strong> (en este caso te ofrecemos la
				distribución Ubuntu). <br /> <br /> Linux suele usarse por usuarios que
				tienen conocimientos más técnicos y por lo tanto quieren tener un
				control más completo de su sistema, si tú no tienes conocimientos
				avanzados tu mejor opción es Windows 😊.
			</span>
		),
	},
	manufacturer: {
		message: () => (
			<span>
				Para navegar en la web a diario, ver Netflix y responder correos
				electrónicos, las CPU Intel y AMD te brindarán un excelente rendimiento
				desde el primer momento. Sin embargo, hay ciertas tareas en las que las
				opciones de un fabricante funcionarán mejor que las del otro 😮:
				<br />
				<br />
				Si buscas trabajar con tu procesador al realizar tareas intensivas de
				múltiples subprocesos –como edición o transcodificación de video, o
				actividades pesadas de múltiples tareas con decenas de pestañas del
				navegador abiertas–, los procesadores de AMD son más capaces en el
				extremo superior y más rentables en cuanto a precio 😎. <br />
				<br /> Los Intel no son malos, pero tendrás que pagar más por el mismo
				rendimiento, aunque puede valer la pena si el Thunderbolt 3 es algo que
				realmente necesitas.
				<span>
					Para más <strong>información sobre Thunderbolt 3</strong> puedes dar
					click <span style={{ marginBottom: ".3rem" }}>👉🏼</span>{" "}
					<a
						href={"https://www.youtube.com/watch?v=dTJOW08DzZw"}
						target="_blank"
						style={{ color: "#0c3b69", fontWeight: "700" }}
					>
						aquí
					</a>
				</span>
				<br />
				<br />
				Si estás trabajando y jugando en tu computadora de escritorio, o incluso
				solo jugando, los procesadores Ryzen 5000 de AMD siguen siendo la mejor
				opción. Todo desde el 5600X hasta el poderoso 5950X ofrecen el mejor
				desempeño en juegos y productividad. Las opciones de Intel se están
				volviendo más accesibles para hacerlas más competitivas, lo que podría
				hacer que valgan la pena, pero para una potencia natural, AMD tiene la
				ventaja 💪🏼.
				<br />
			</span>
		),
	},
	challenges: {
		message: () => (
			<span>
				Para que el equipo rinda según tus necesidades, debo conocer las tareas
				que realizarás con él. De esta manera te aseguro que el equipo será el
				ideal para ti 💪🏼. <br /> <br />
				<ul>
					<li>
						<strong>Básico:</strong> <br /> En esta opción buscaré ofrecerte un
						equipo que ofrezca el rendimiento necesario para que puedas realizar
						tus tareas de oficina 👨🏼‍💼: (manejar word, excel, etc.), navegar en la
						red y tener tus conferencias sin ningún inconveniente. <br />
						Esta opción también es ideal para estudiantes 👩🏼‍🎓.
					</li>
					<br />
					<br />
					<li>
						<strong>Gamer:</strong> <br /> Esta opción está enfocada en aquellos
						amantes de los videojuegos 🎮. Por lo tanto te aseguro que
						disfrutarás de unos gráficos excelentes, un procesador que te
						ofrecerá un rendimiento óptimo, capacidad de cooling para mantener
						tu equipo en una temperatura fresca, entre muchas cosas más.
					</li>
					<br />
					<br />
					<li>
						<strong>Programación y sistemas:</strong> <br /> Opción enfocada a
						los desarrolladores de software y científicos de datos 👨🏼‍💻. Al elegir
						estas opciones me aseguraré de brindarte un equipo que sea eficaz al
						mometo de ejecutar emuladores, máquinas virtuales, o compilar tú
						código.
					</li>
					<br />
					<br />
					<li>
						<strong>Edición y diseño 🎨:</strong> <br /> Si tú trabajo se enfoca
						en la edición de vídeo o fotografía, o en la creación de interfaces
						UI/UX, te ofreceré un equipo potente para que tus aplicaciones
						trabajen de manera óptima, una tarjeta gráfica excelente para el
						procesamiento de tus imágenes y suficiente memoria RAM para que
						puedas trabajar con múltiples programas a la vez.
					</li>
				</ul>
			</span>
		),
	},
	bullet: {
		message: () => (
			<span>
				¡Ya casi terminas! Tu presupuesto lo necesito para elegir los
				componentes en relación calidad-precio, de esta manera te aseguro que{" "}
				<strong>¡tu inversión será aprovechada al máximo!</strong> 💰. <br />{" "}
			</span>
		),
	},
	accessories: {
		message: () => (
			<span>
				Indícame qué accesorios deseas que incluya a tu nueva PC 😊. <br />{" "}
			</span>
		),
	},
	finalStep: {
		message: () => (
			<span>
				<span
					style={{ display: "block", textAlign: "center", fontWeight: "bold" }}
				>
					¡Listo! 🎉🎉🎉.
				</span>
				<br />
				<br />
				He seleccionado los componentes que se adaptan a tus necesidades 🥳.{" "}
				<br />
				<br /> Si observas que, por ejemplo, hay dos procesadores 🤨, se debe a
				que ambos se ajustan a tus necesidades, pero tú puedes elegir cuál de
				los dos deseas. <br /> Ambos productos te son útiles, asi que,
				cualquiera que sea tu elección, será la correcta 😊.
				<br />
				<br />
				Puedes aumentar las unidades de los productos o eliminar el que no
				desees en la pantalla del carrito, asi que, si te gustan estos
				resultados pulsa en el botón <strong>"Añadir al carrito"</strong> que se
				encuentra en la parte inferior, o si no estás agusto con el resultado,
				pulsa en <strong>"Realizar otro quiz"</strong> 😁.
			</span>
		),
	},
	login: {
		message: () => (
			<span>
				¡Hola! Soy Desky 👋🏼, para poder darte una ayuda personalizada debes
				iniciar sesión. <br />
				<br /> Si aún no tienes una cuenta pulsa en{" "}
				<strong>"Crear cuenta"</strong>. Recuerda que también puedes usar tu
				cuenta de Google 😊.
			</span>
		),
	},
	desks: {
		messages: () => (
			<>
				<h3 style={{ textAlign: 'center' }}>Especificaciones generales:</h3>
				<ul>
					<ListItem>Mesa hecha de plywood.</ListItem>
					<ListItem>
						Instalación que oculta cables y resalta la belleza de las piezas
						instaladas.
					</ListItem>
					<ListItem>4 Rodos de acero inoxidable con bloqueo de rotación y avance.</ListItem>
					<ListItem>
						8 ventiladores, 4 de entrada de aire y 4 de salida, diseño del
						airflow sin obstáculos aseguran frescura.
					</ListItem>
					<ListItem>Luces RGB multicolor a control remoto grande, modelo 5050.</ListItem>
					<ListItem>
						Vidrio de 5mm de grosor, que queda a ras del largo y ancho del
						escritorio.
					</ListItem>
					<ListItem>
						Capacidad de expansión, más allá de cualquier case disponible en el
						mercado.
					</ListItem>
					<ListItem>
						Instalación eléctrica independiente a instalación de pc incluida.
					</ListItem>
					<ListItem>Botón iluminado de encendido de ventilación y RGB.</ListItem>
					<ListItem>
						Filtros de malla esféricos en la entrada de aire, permiten filtrar
						el polvo eficientemente, a su vez hace que limpiarlos sea más
						práctico.
					</ListItem>
					<ListItem>
						Bocinas de alta fidelidad de 4cm de diámetro, 4 watts de salida por
						parlante.
					</ListItem>
					<ListItem>
						Panel frontal de encendido y reseteo de cpu, estilo minimalista.
					</ListItem>
					<ListItem>
						Refuerzo de estructura en el diseño, ¡cero vibraciones al utilizar
						el escritorio!
					</ListItem>
					<ListItem>
						Sello completo entre el vidrio y el interior del escritorio aseguran
						tu inversión, ¡los accidentes pasan!
					</ListItem>
				</ul>
			</>
		),
	},
};
